package com.example.postlayoutproject

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.widget.ImageView
import androidx.core.content.FileProvider
import com.example.postlayoutproject.ui.fragments.PostDetailFragment
import java.io.File
import java.io.FileOutputStream


const val CHILD_IMAGES = "/images"

internal val Any.TAG: String
    get() {
        return if (!javaClass.isAnonymousClass) {
            val name = javaClass.simpleName
            if (name.length <= 25) name else name.substring(0, 25)
        } else {
            val name = javaClass.name
            if (name.length <= 25) name else name.substring(name.length - 25, name.length)
        }
    }

internal interface ViewModelContract<ACTION> {
    fun process(viewAction: ACTION)
}

class NoObserverAttachedException(message: String) : Exception(message)

fun ImageView.obtainBitmap(): Bitmap? =
    if (drawable is BitmapDrawable) {
        (drawable as BitmapDrawable).bitmap
    } else {
        null
    }

fun Bitmap.saveBitmapForSharing(): Uri {
    val dir = File(App.instance.cacheDir, CHILD_IMAGES)
    dir.mkdir()
    val file = File(
        dir,
        PostDetailFragment.SHARED_IMAGE_TITLE + System.currentTimeMillis() + PostDetailFragment.IMAGE_EXTENSION_PNG
    )
    val out = FileOutputStream(file)
    compress(Bitmap.CompressFormat.PNG, 90, out)
    out.close()

    return FileProvider.getUriForFile(
        App.instance,
        PostDetailFragment.AUTHORITY,
        file
    )
}
