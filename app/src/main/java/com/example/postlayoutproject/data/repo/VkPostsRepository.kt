package com.example.postlayoutproject.data.repo

import android.content.SharedPreferences
import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.rxjava2.flowable
import com.example.postlayoutproject.data.database.DatabasePost
import com.example.postlayoutproject.data.database.dao.PostDao
import com.example.postlayoutproject.data.models.Post
import com.example.postlayoutproject.data.models.datatransferobjects.BaseLikes
import com.example.postlayoutproject.data.models.datatransferobjects.Comment
import com.example.postlayoutproject.data.models.datatransferobjects.CommentSendBase
import com.example.postlayoutproject.data.models.datatransferobjects.UserPostSendBase
import com.example.postlayoutproject.data.network.TokenStorage
import com.example.postlayoutproject.data.network.VkApiService
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class VkPostsRepository @Inject constructor(
    private val postDao: PostDao,
    vkApiService: VkApiService,
    tokenStorage: TokenStorage,
    private val sharedPreferences: SharedPreferences,
    private val favoriteSubject: PublishSubject<Boolean>
) : VkProfileRepository(vkApiService, tokenStorage) {

    private var favoritePosts = hashMapOf<Int, Boolean>()

    @OptIn(ExperimentalPagingApi::class)
    fun fetchPosts(): Flowable<PagingData<DatabasePost>> {
        return Pager(
            PagingConfig(pageSize = 20, enablePlaceholders = false, prefetchDistance = 5),
            remoteMediator = VkPostsRemoteMediator(
                vkApiService,
                sharedPreferences,
                postDao,
                tokenStorage,
                favoritePosts
            ),
            pagingSourceFactory = { postDao.getPosts() }
        ).flowable
            .doOnNext { invalidateHasFavorites() }
    }

    fun getFavoritePosts(): Flowable<PagingData<DatabasePost>> = Pager(
        PagingConfig(pageSize = 20, enablePlaceholders = false, prefetchDistance = 5),
        pagingSourceFactory = { postDao.getFavoritePosts(true) }).flowable

    @OptIn(ExperimentalPagingApi::class)
    fun fetchComments(post: Post): Flowable<PagingData<Comment>> = Pager(
        PagingConfig(pageSize = 30, enablePlaceholders = false, prefetchDistance = 3),
    ) {
        CommentsPagingSource(
            network = vkApiService,
            tokenStorage = tokenStorage,
            ownerId = post.sourceId,
            postId = post.postId
        )
    }.flowable

    fun publishComment(
        message: String,
        ownerId: Int,
        postId: Int,
        guid: String
    ): Single<CommentSendBase> =
        vkApiService.createComment(message = message, accessToken = tokenStorage.getToken(), ownerId = ownerId, postId = postId, guid = guid)

    fun removePost(postId: Int, ownerId: Int): Observable<Int> {
        favoritePosts.remove(postId)
        invalidateHasFavorites()
        return vkApiService.ignorePost(postId = postId, ownerId = ownerId, tokenStorage.getToken())
    }

    fun removePostFromDb(postId: Int): Completable {
        return Completable.fromAction { postDao.deletePost(postId) }
    }

    fun setFavorite(postId: Int, ownerId: Int, isFavorite: Boolean): Completable =
        Single.fromCallable {
            postDao.update(!isFavorite, postId)
            favoritePosts[postId] = !isFavorite
            invalidateHasFavorites()
        }.flatMap {
            setIsFavorite(postId, ownerId, isFavorite)
        }.ignoreElement()

    private fun setIsFavorite(
        postId: Int,
        ownerId: Int,
        isFavorite: Boolean
    ): Single<BaseLikes> {
        return if (!isFavorite) {
            vkApiService.likePost(postId = postId, ownerId = ownerId, tokenStorage.getToken())
        } else {
            vkApiService.dislikePost(postId = postId, ownerId = ownerId, tokenStorage.getToken())
        }
    }

    fun invalidateHasFavorites() {
        favoriteSubject.onNext(favoritePosts.values.contains(true))
    }

    fun publishPost(message: String, guid: String): Single<UserPostSendBase> = vkApiService.createPost(
        message = message,
        accessToken = tokenStorage.getToken(),
        guid = guid
    )

}
