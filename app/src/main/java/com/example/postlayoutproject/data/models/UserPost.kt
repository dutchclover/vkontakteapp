package com.example.postlayoutproject.data.models

import com.example.postlayoutproject.data.models.datatransferobjects.*
import com.google.gson.annotations.SerializedName

data class UserPost (
    @SerializedName("id") val id : Int,
    @SerializedName("from_id") val fromId : Int,
    @SerializedName("owner_id") val ownerId : Int,
    @SerializedName("date") val date : Long,
    val formattedDate: String,
    val repostedText: String,
    val avatarImgUrl: String,
    val groupOrUserName: String?,
    val photoLink: String?,
    val repostedAvatarImgUrl: String,
    val repostedGroupOrUserName: String?,
    val repostedFormattedDate: String?,
    @SerializedName("post_type") val postType : String,
    @SerializedName("text") val text : String,
    @SerializedName("copy_history") val copyHistory : List<CopyHistory>?,
    @SerializedName("can_delete") val canDelete : Int,
    @SerializedName("can_pin") val canPin : Int,
    @SerializedName("can_archive") val canArchive : Boolean,
    @SerializedName("is_archived") val isArchived : Boolean,
    @SerializedName("post_source") val postSource : PostSource,
    @SerializedName("comments") val comments : Comments,
    @SerializedName("likes") val likes : Likes,
    @SerializedName("reposts") val reposts : Reposts,
    @SerializedName("is_favorite") val isFavorite : Boolean,
    @SerializedName("attachments") val attachments: List<Attachments>?,
)