package com.example.postlayoutproject.data.models.datatransferobjects

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Country (
    @SerializedName("id") val id : Int,
    @SerializedName("title") val title : String
): Parcelable