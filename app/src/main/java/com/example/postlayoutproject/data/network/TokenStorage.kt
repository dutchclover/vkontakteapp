package com.example.postlayoutproject.data.network

import android.content.SharedPreferences
import javax.inject.Inject



class TokenStorage @Inject constructor(private val sharedPreferences: SharedPreferences) {

    private var token: String? = null

    init {
        token = sharedPreferences.getString(TOKEN, null)
        println("token is $token")
    }

    fun saveToken(token: String?) {
        this.token = token
        sharedPreferences.edit().putString(TOKEN, token).apply()
    }

    fun getToken() = token

    companion object {
        const val TOKEN = "VKToken"
    }
}