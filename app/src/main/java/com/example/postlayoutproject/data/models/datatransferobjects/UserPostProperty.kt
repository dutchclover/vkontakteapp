package com.example.postlayoutproject.data.models.datatransferobjects

import com.example.postlayoutproject.data.models.UserPost
import com.google.gson.annotations.SerializedName
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.util.*

data class UserPostProperty(
    @SerializedName("id") val id: Int,
    @SerializedName("from_id") val fromId: Int,
    @SerializedName("owner_id") val ownerId: Int,
    @SerializedName("date") val date: Long,
    val formattedDate: DateTime,
    val repostedText: String,
    val avatarImgUrl: String,
    val groupOrUserName: String?,
    val photoLink: String?,
    val repostedAvatarImgUrl: String,
    val repostedGroupOrUserName: String?,
    val repostedFormattedDate: DateTime?,
    @SerializedName("post_type") val postType: String,
    @SerializedName("text") val text: String,
    @SerializedName("copy_history") val copyHistory: List<CopyHistory>?,
    @SerializedName("can_delete") val canDelete: Int,
    @SerializedName("can_pin") val canPin: Int,
    @SerializedName("can_archive") val canArchive: Boolean,
    @SerializedName("is_archived") val isArchived: Boolean,
    @SerializedName("post_source") val postSource: PostSource,
    @SerializedName("comments") val comments: Comments,
    @SerializedName("likes") val likes: Likes,
    @SerializedName("reposts") val reposts: Reposts,
    @SerializedName("attachments") val attachments: List<Attachments>?,
    @SerializedName("is_favorite") val isFavorite: Boolean
)

fun List<UserPostProperty>.asDomainModel(): List<UserPost> {
    return map {
        UserPost(
            id = it.id,
            fromId = it.fromId,
            ownerId = it.ownerId,
            date = it.date,
            formattedDate = it.formattedDate.toString(dateFormat),
            repostedText = it.repostedText,
            avatarImgUrl = it.avatarImgUrl,
            groupOrUserName = it.groupOrUserName,
            photoLink = it.photoLink,
            repostedAvatarImgUrl = it.repostedAvatarImgUrl,
            repostedGroupOrUserName = it.repostedGroupOrUserName,
            repostedFormattedDate = it.repostedFormattedDate?.toString(dateFormat),
            postType = it.postType,
            text = it.text,
            copyHistory = it.copyHistory,
            canDelete = it.canDelete,
            canPin = it.canPin,
            canArchive = it.canArchive,
            isArchived = it.isArchived,
            postSource = it.postSource,
            comments = it.comments,
            likes = it.likes,
            reposts = it.reposts,
            isFavorite = it.isFavorite,
            attachments = it.attachments
        )
    }
}

private val dateFormat = DateTimeFormat.forPattern( "dd MMMM YYYY HH:mm").withLocale(Locale("ru"))