package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class CountryResponse (
    @SerializedName("id") val id : Int,
    @SerializedName("title") val countryName : String
)