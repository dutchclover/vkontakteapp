package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class CityResponse(
    @SerializedName("id") val id : Int,
    @SerializedName("title") val cityName : String
)