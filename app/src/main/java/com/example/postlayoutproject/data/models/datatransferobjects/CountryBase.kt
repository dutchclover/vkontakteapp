package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class CountryBase(
    @SerializedName("response") val response : List<CountryResponse>
)