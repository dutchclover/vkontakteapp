package com.example.postlayoutproject.data.models.datatransferobjects

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class City (
    @SerializedName("id") val id : Int?,
    val cityFromID: String?,
    @SerializedName("title") val title : String?
): Parcelable