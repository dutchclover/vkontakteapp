package com.example.postlayoutproject.data.repo

import androidx.paging.rxjava2.RxPagingSource
import com.example.postlayoutproject.data.models.UserPost
import com.example.postlayoutproject.data.models.datatransferobjects.GroupProperty
import com.example.postlayoutproject.data.models.datatransferobjects.Profile
import com.example.postlayoutproject.data.models.datatransferobjects.UserPostProperty
import com.example.postlayoutproject.data.models.datatransferobjects.asDomainModel
import com.example.postlayoutproject.data.network.TokenStorage
import com.example.postlayoutproject.data.network.VkApiService
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import retrofit2.HttpException
import java.io.IOException
import java.util.*

class UserPostsPagingSource(
    private val network: VkApiService,
    private val tokenStorage: TokenStorage,
) : RxPagingSource<Int, UserPost>() {

    override fun loadSingle(
        params: LoadParams<Int>
    ): Single<LoadResult<Int, UserPost>> {
        val position = params.key ?: 0
        return network
            .getUserPosts(tokenStorage.getToken(), params.key, params.loadSize)
            .subscribeOn(Schedulers.io())
            .map<LoadResult<Int, UserPost>> { base ->
                val formattedList = formatDateAndAvatar(base.response.items)
                val listWithImages = setImageLink(formattedList)
                val userPostsList = attachPostToUser(listWithImages, base.response.profiles)
                val mappedList = mapRepost(
                    userPostsList,
                    base.response.groups,
                    base.response.profiles
                ).sortedByDescending { it.date }
                    .asDomainModel()

                var i = 0
                mappedList.map { println("loadSingle key ${params.key} .userPost i = ${++i} date ${Date(it.date)} id ${it.id}") }

                LoadResult.Page(
                    data = mappedList,
                    prevKey = null,
                    nextKey = if (mappedList.isEmpty() || ((position + mappedList.size) >= base.response.count))
                        null
                    else position + mappedList.size + 1
                )
            }.onErrorReturn { e ->
                when (e) {
                    is IOException -> LoadResult.Error(e)
                    is HttpException -> LoadResult.Error(e)
                    else -> throw e
                }
            }
    }

    private fun formatDateAndAvatar(posts: List<UserPostProperty>): List<UserPostProperty> {
        val mutableList = mutableListOf<UserPostProperty>()
        posts.map { post ->
            val formattedDate =
                DateTime(post.date * MULTIPLIER, DateTimeZone.forOffsetHours(OFFSET_HOURS))
            val repostedFormattedDate = if (post.copyHistory != null) DateTime(
                post.copyHistory.first().date * MULTIPLIER,
                DateTimeZone.UTC
            ) else null
            val repostedText =
                if (!post.copyHistory.isNullOrEmpty()) post.copyHistory.first().text else ""
            val copy = post.copy(
                repostedText = repostedText,
                formattedDate = formattedDate,
                repostedFormattedDate = repostedFormattedDate,
                avatarImgUrl = "",
                repostedAvatarImgUrl = ""
            )
            mutableList.add(copy)
        }
        return mutableList
    }

    private fun attachPostToUser(
        posts: List<UserPostProperty>,
        users: List<Profile>
    ): List<UserPostProperty> {
        val mutableList = mutableListOf<UserPostProperty>()
        posts.forEach { post ->
            var groupname: String
            var avatarImg: String
            users.find { user ->
                user.id == post.fromId
            }?.let {
                groupname = (it.firstName + " " + it.lastName)
                avatarImg = it.photo100
                mutableList.add(
                    post.copy(
                        groupOrUserName = groupname,
                        avatarImgUrl = avatarImg
                    )
                )
            } ?: run {
                groupname = ""
                avatarImg = ""
                mutableList.add(
                    post.copy(
                        groupOrUserName = groupname,
                        avatarImgUrl = avatarImg
                    )
                )
            }
        }
        return mutableList
    }

    private fun mapRepost(
        posts: List<UserPostProperty>,
        groups: List<GroupProperty>,
        users: List<Profile>
    ): List<UserPostProperty> {
        val mutableList = mutableListOf<UserPostProperty>()
        var name: String
        var avatarImg: String
        posts.forEach { post ->
            groups.find { group ->
                -group.id == post.copyHistory?.first()?.ownerId
            }?.let {
                name = it.name
                avatarImg = it.photo100
                mutableList.add(
                    post.copy(
                        repostedGroupOrUserName = name,
                        repostedAvatarImgUrl = avatarImg
                    )
                )
            } ?: users.find { user ->
                user.id == post.copyHistory?.first()?.ownerId
            }?.let {
                name = (it.firstName + " " + it.lastName)
                avatarImg = it.photo100
                mutableList.add(
                    post.copy(
                        repostedGroupOrUserName = name,
                        repostedAvatarImgUrl = avatarImg
                    )
                )
            } ?: run {
                mutableList.add(
                    post.copy(
                        repostedGroupOrUserName = "",
                        repostedAvatarImgUrl = ""
                    )
                )
            }
        }
        return mutableList
    }

    private fun setImageLink(list: List<UserPostProperty>): List<UserPostProperty> {
        val mutableList = mutableListOf<UserPostProperty>()
        list.forEach { post ->
            if (post.attachments?.first() != null && post.attachments.first().photo != null) {
                post.attachments.first {
                    it.photo.let { photo ->
                        val photoLink =
                            if (!photo?.sizes.isNullOrEmpty()) photo!!.sizes[4].url else null
                        val copy = post.copy(photoLink = photoLink)
                        mutableList.add(copy)
                    }
                }
            } else if (post.copyHistory != null && post.copyHistory.first().attachments != null) {
                post.copyHistory.first().attachments?.first { attachment ->
                    attachment.photo.let { photo ->
                        val photoLink =
                            if (!photo?.sizes.isNullOrEmpty()) photo!!.sizes[4].url else ""
                        val copy = post.copy(photoLink = photoLink)
                        mutableList.add(copy)
                    }
                }
            } else {
                val copy = post.copy(photoLink = "")
                mutableList.add(copy)
            }
        }
        return mutableList
    }

    companion object {
        const val OFFSET_HOURS = 4
        const val MULTIPLIER = 1000
    }
}