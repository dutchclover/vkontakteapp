package com.example.postlayoutproject.data.models.datatransferobjects

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Career (
    @SerializedName("city_id") val cityId : Int?,
    @SerializedName("city_name") val cityName : String?,
    @SerializedName("group_id") val groupId : Int?,
    @SerializedName("company") val company : String?,
    @SerializedName("country_id") val countryId : Int?,
    @SerializedName("from") val from : Int?,
    @SerializedName("until") val until : Int?,
    @SerializedName("position") val position : String?
): Parcelable