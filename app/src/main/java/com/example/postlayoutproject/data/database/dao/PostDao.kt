package com.example.postlayoutproject.data.database.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.postlayoutproject.data.database.DatabasePost

@Dao
interface PostDao {
    @Query("select * from databasepost order by date desc")
    fun getPosts(): PagingSource<Int, DatabasePost>

    @Query("select * from databasepost where is_favorite = :isFavorite order by date desc")
    fun getFavoritePosts(isFavorite: Boolean): PagingSource<Int, DatabasePost>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg posts: DatabasePost)

    @Query("update databasepost SET is_favorite = :isFavorite WHERE  post_id = :postId")
    fun update(isFavorite: Boolean, postId: Int)

    @Query("delete from databasepost WHERE post_id = :postId ")
    fun deletePost(postId: Int)

    @Query("delete from databasepost")
    fun clearAll()

}