package com.example.postlayoutproject.data.repo

import androidx.paging.rxjava2.RxPagingSource
import com.example.postlayoutproject.data.models.datatransferobjects.BaseComments
import com.example.postlayoutproject.data.models.datatransferobjects.Comment
import com.example.postlayoutproject.data.models.datatransferobjects.GroupProperty
import com.example.postlayoutproject.data.models.datatransferobjects.Profile
import com.example.postlayoutproject.data.network.TokenStorage
import com.example.postlayoutproject.data.network.VkApiService
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import retrofit2.HttpException
import java.io.IOException
import java.util.*

class CommentsPagingSource(
    private val network: VkApiService,
    private val tokenStorage: TokenStorage,
    private val ownerId: Int,
    private val postId: Int
) : RxPagingSource<Int, Comment>() {

    override fun loadSingle(
        params: LoadParams<Int>
    ): Single<LoadResult<Int, Comment>> {
        val position = params.key
        return network
            .getComments(tokenStorage.getToken(), ownerId, postId, position, params.loadSize)
            .subscribeOn(Schedulers.io())
            .map<LoadResult<Int, Comment>> { response: BaseComments ->
                val items = response.commentsResponse.items.filter { it.id != params.key }
                val commentsList: List<Comment> = getCommentDate(items).sortedBy { it.date }
                val mappedCommentsList = attachUserOrGroup(
                    commentsList,
                    response.commentsResponse.profiles,
                    response.commentsResponse.groups
                )
                LoadResult.Page(
                    data = mappedCommentsList,
                    prevKey = null,
                    nextKey = if (commentsList.isEmpty() || commentsList.first().id == params.key) null else commentsList.first().id
                )
            }.onErrorReturn { e ->
                when (e) {
                    is IOException -> LoadResult.Error(e)
                    is HttpException -> LoadResult.Error(e)
                    else -> throw e
                }
            }
    }

    private fun getCommentDate(comments: List<Comment>): List<Comment> {
        return if (comments.isNotEmpty()) {
            val mutableList = mutableListOf<Comment>()
            comments.map { comment ->
                val formattedDate = DateTime(comment.date * MULTIPLIER, DateTimeZone.forOffsetHours(
                    OFFSET_HOURS))
                val copy = comment.copy(formattedDate = formatDate(formattedDate))
                mutableList.add(copy)
            }
            mutableList
        } else emptyList()
    }

    private fun attachUserOrGroup(
        comments: List<Comment>,
        users: List<Profile>,
        groups: List<GroupProperty>
    ): List<Comment> {
        val mutableList = mutableListOf<Comment>()
        comments.forEach { comment ->
            var name: String
            var firstName: String
            var lastName: String
            var avatarImg: String
            users.find { user ->
                user.id == comment.fromId
            }?.let {
                firstName = it.firstName
                lastName = it.lastName
                avatarImg = it.photo50
                mutableList.add(
                    comment.copy(
                        firstName = firstName,
                        lastName = lastName,
                        photo = avatarImg
                    )
                )
            } ?: groups.find { group ->
                group.id == comment.fromId
            }?.let {
                name = it.name
                avatarImg = it.photo50
                mutableList.add(
                    comment.copy(
                        name = name,
                        photo = avatarImg
                    )
                )
            }?.run {
                name = ""
                avatarImg = ""
                mutableList.add(
                    comment.copy(
                        name = name,
                        photo = avatarImg
                    )
                )
            }
        }
        return mutableList
    }

    companion object {
        const val OFFSET_HOURS = 4
        const val TODAY = "Сегодня"
        const val YESTERDAY = "Вчера"
        const val PATTERN = "dd MMMM YYYY HH:mm"
        const val RUSSIAN_LOCALE = "ru"
        const val MULTIPLIER = 1000
    }

    val dateFormat: DateTimeFormatter = DateTimeFormat.forPattern(PATTERN).withLocale(
        Locale(RUSSIAN_LOCALE)
    )

    fun formatDate(date: DateTime): String {
        val currentDate = DateTime.now().toDateTime(DateTimeZone.forOffsetHours(
            OFFSET_HOURS))
        return when {
            currentDate.dayOfYear == date.dayOfYear ->
                TODAY + " в " + date.hourOfDay().asString + "." + date.minuteOfHour().asString
            (currentDate.dayOfYear - date.dayOfYear) == 1 ->
                YESTERDAY + " в " + date.hourOfDay().asString + "." + date.minuteOfHour().asString
            else -> date.toString(dateFormat)
        }
    }
}