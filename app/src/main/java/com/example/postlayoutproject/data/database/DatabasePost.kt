package com.example.postlayoutproject.data.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.postlayoutproject.data.models.Post

@Entity
data class DatabasePost(
    @ColumnInfo(name = "source_id") val sourceId: Int,
    @ColumnInfo(name = "date") val date: Long,
    @ColumnInfo val formattedDate: String,
    @ColumnInfo val avatarImgUrl: String,
    @ColumnInfo val groupName: String?,
    @ColumnInfo val photoLink: String?,
    @ColumnInfo(name = "text") val text: String,
    @ColumnInfo(name = "comments") val comments: Int,
    @ColumnInfo(name = "likes") val likes: Int,
    @ColumnInfo(name = "reposts") val reposts: Int,
    @ColumnInfo(name = "is_favorite") val isFavorite: Boolean,
    @PrimaryKey @ColumnInfo(name = "post_id") val postId: Int,
    @ColumnInfo(name = "type") val type: String
)

fun DatabasePost.asDomainModel(): Post {
    return Post(
            sourceId = sourceId,
            date = date,
            formattedDate = formattedDate,
            avatarImgUrl = avatarImgUrl,
            groupName = groupName,
            photoLink = photoLink,
            text = text,
            comments = comments,
            likes = likes,
            reposts = reposts,
            isFavorite = isFavorite,
            postId = postId,
            type = type
        )
    }
