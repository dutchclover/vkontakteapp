package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class LikesResponse(
    @SerializedName("likes") val likes: Int
)
