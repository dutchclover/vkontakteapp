package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class CopyHistory(
    @SerializedName("id") val id: Int,
    @SerializedName("owner_id") val ownerId: Int,
    @SerializedName("from_id") val fromId: Int,
    @SerializedName("date") val date: Long,
    @SerializedName("post_type") val postType: String,
    @SerializedName("text") val text: String,
    @SerializedName("attachments") val attachments: List<Attachments>?,
    @SerializedName("post_source") val postSource: PostSource
)