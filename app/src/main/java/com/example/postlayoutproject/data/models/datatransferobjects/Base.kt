package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class Base (
    @SerializedName("response") val response : Response
)