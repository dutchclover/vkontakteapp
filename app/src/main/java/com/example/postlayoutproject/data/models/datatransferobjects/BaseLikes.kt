package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class BaseLikes(
    @SerializedName("response") val likesResponse : LikesResponse
)
