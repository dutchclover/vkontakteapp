package com.example.postlayoutproject.data.repo

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.rxjava2.flowable
import com.example.postlayoutproject.data.models.UserPost
import com.example.postlayoutproject.data.models.datatransferobjects.Career
import com.example.postlayoutproject.data.models.datatransferobjects.PreparedCareer
import com.example.postlayoutproject.data.models.datatransferobjects.Profile
import com.example.postlayoutproject.data.network.TokenStorage
import com.example.postlayoutproject.data.network.VkApiService
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.functions.Function3

open class VkProfileRepository(
    protected val vkApiService: VkApiService,
    protected val tokenStorage: TokenStorage
) {

    fun fetchProfile(): Single<List<Profile>> = vkApiService.getProfile(tokenStorage.getToken())
        .map { it.profileList }
        .flatMap { profiles ->
            Single.zip(
                loadCities(profiles.getCityIds()),
                loadCompany(profiles.getCompanyIds()),
                loadCountries(profiles.getCountryIds()),
                Function3<Map<Int, String>, Map<Int, String>, Map<Int, String>, List<Profile>> { citiesMap, companiesMap, countriesMap ->
                    return@Function3 profiles.also {
                        it.prepareCareers(citiesMap, companiesMap, countriesMap)
                    }
                })
        }

    private fun List<Profile>.prepareCareers(
        citiesMap: Map<Int, String>,
        companiesMap: Map<Int, String>,
        countriesMap: Map<Int, String>
    ) = forEach { profile ->
        profile.preparedCareers = mutableListOf()
        profile.career.forEach { carrier ->
            profile.preparedCareers.add(
                PreparedCareer(
                    carrier?.getCareerCity(citiesMap),
                    carrier?.getCountry(countriesMap),
                    carrier?.getCareerCompany(companiesMap),
                    from = carrier?.from,
                    until = carrier?.until,
                    position = carrier?.position
                )
            )
        }
    }

    private fun loadCities(cities: Set<String>) = if (cities.isNotEmpty())
        vkApiService.getCity(cities.toList().joinToString(","), tokenStorage.getToken()).map { cityBase ->
            val citiesMap = hashMapOf<Int, String>()
            cityBase.response.forEach {
                citiesMap[it.id] = it.cityName
            }
            citiesMap
        }
    else Single.just(emptyMap<Int, String>())

    private fun loadCountries(countries: Set<String>) = if (countries.isNotEmpty())
        vkApiService.getCountry(countries.toList().joinToString(","), tokenStorage.getToken())
            .map { countryBase ->
                val countriesMap = hashMapOf<Int, String>()
                countryBase.response.forEach {
                    countriesMap[it.id] = it.countryName
                }
                countriesMap
            }
    else Single.just(emptyMap<Int, String>())


    private fun loadCompany(companies: Set<String>) = if (companies.isNotEmpty())
        vkApiService.getCompany(companies.toList().joinToString(","), tokenStorage.getToken())
            .map { groupBase ->
                val companiesMap = hashMapOf<Int, String>()
                groupBase.response.forEach {
                    companiesMap[it.id] = it.name
                }
                companiesMap
            }
    else Single.just(emptyMap<Int, String>())

    private fun Career.getCareerCity(citiesMap: Map<Int, String>) =
        if (cityName == null && citiesMap.containsKey(cityId))
            citiesMap.getValue(cityId!!)
        else
            cityName

    private fun Career.getCareerCompany(companiesMap: Map<Int, String>) =
        if (company == null && companiesMap.containsKey(groupId))
            companiesMap.getValue(groupId!!)
        else
            company

    private fun Career.getCountry(countriesMap: Map<Int, String>) = countryId?.let { countriesMap.getValue(it) }

    private fun List<Profile>.getCityIds(): Set<String> {
        val cityIds = hashSetOf<String>()
        forEach { profile ->
            profile.career.forEach { carrier ->
                carrier?.cityId?.let { cityId ->
                    cityIds.add(cityId.toString())
                }
            }
        }
        return cityIds
    }

    private fun List<Profile>.getCountryIds(): Set<String> {
        val countryIds = hashSetOf<String>()
        forEach { profile ->
            profile.career.forEach { carrier ->
                carrier?.countryId?.let { countryId ->
                    countryIds.add(countryId.toString())
                }
            }
        }
        return countryIds
    }

    private fun List<Profile>.getCompanyIds(): Set<String> {
        val companyIds = hashSetOf<String>()
        forEach { profile ->
            profile.career.forEach { carrier ->
                carrier?.groupId?.let { groupId ->
                    companyIds.add(groupId.toString())
                }
            }
        }
        return companyIds
    }

    fun getUserPosts(): Flowable<PagingData<UserPost>> = Pager(
        PagingConfig(pageSize = 20, enablePlaceholders = false, prefetchDistance = 3),
    ) {
        UserPostsPagingSource(
            network = vkApiService,
            tokenStorage = tokenStorage,
        )
    }.flowable
}