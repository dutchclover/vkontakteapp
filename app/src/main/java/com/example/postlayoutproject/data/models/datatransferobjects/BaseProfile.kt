package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class BaseProfile(
    @SerializedName("response") val profileList : List<Profile>
)