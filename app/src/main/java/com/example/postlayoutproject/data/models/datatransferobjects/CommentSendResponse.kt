package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class CommentSendResponse (
    @SerializedName("comment_id") val commentId : Int,
)