package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class Response (
    @SerializedName("items") val items : List<PostProperty>,
    @SerializedName("groups") val groups : List<GroupProperty>,
    @SerializedName("next_from") val nextKey : String?,
    @SerializedName("profiles") val profiles : List<Profile>,
)