package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class BaseComments(
@SerializedName("response") val commentsResponse : CommentsResponse
)
