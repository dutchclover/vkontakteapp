package com.example.postlayoutproject.data.repo

import android.content.SharedPreferences
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.rxjava2.RxRemoteMediator
import com.example.postlayoutproject.data.database.DatabasePost
import com.example.postlayoutproject.data.database.dao.PostDao
import com.example.postlayoutproject.data.models.asDatabaseModel
import com.example.postlayoutproject.data.models.datatransferobjects.GroupProperty
import com.example.postlayoutproject.data.models.datatransferobjects.PostProperty
import com.example.postlayoutproject.data.models.datatransferobjects.Profile
import com.example.postlayoutproject.data.models.datatransferobjects.asDomainModel
import com.example.postlayoutproject.data.network.TokenStorage
import com.example.postlayoutproject.data.network.VkApiService
import io.reactivex.Single
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import retrofit2.HttpException
import java.io.IOException

@OptIn(ExperimentalPagingApi::class)
class VkPostsRemoteMediator(
    private val networkService: VkApiService,
    private val sharedPreferences: SharedPreferences,
    private val postDao: PostDao,
    private val tokenStorage: TokenStorage,
    private val favoritePosts: MutableMap<Int, Boolean>
) : RxRemoteMediator<Int, DatabasePost>() {

    private var nextPageKey: String? = sharedPreferences.getString(NEXT_PAGE_KEY , null)
        set(value) {
            field = value
            sharedPreferences.edit().putString(NEXT_PAGE_KEY, value).apply()
        }

    override fun loadSingle(
        loadType: LoadType,
        state: PagingState<Int, DatabasePost>
    ): Single<MediatorResult> = when (loadType) {
        LoadType.REFRESH ->  fetchPosts()
        LoadType.PREPEND -> Single.just(MediatorResult.Success(endOfPaginationReached = true))
        LoadType.APPEND -> {
            state.lastItemOrNull()?.let {
                fetchPosts(nextPageKey).onErrorResumeNext {
                    return@onErrorResumeNext Single.just<MediatorResult>(MediatorResult.Error(it))
                }
            } ?: Single.just(MediatorResult.Success(endOfPaginationReached = true))
        }
    }

    private fun fetchPosts(pagingKey: String? = null): Single<MediatorResult> =
        networkService.getPostsAsync(startFrom = pagingKey, loadSize = 20, accessToken = tokenStorage.getToken())
            .map<MediatorResult> { base ->
                val formattedList = formatDateAndAvatar(base.response.items)
                val preparedList = prepareFavorites(formattedList)
                val listWithPhotos = setPhotoLink(preparedList)
                attachPostToGroup(listWithPhotos, base.response.groups, base.response.profiles)
                    .sortedByDescending { it.date }
                    .let {
                        postDao.insertAll(*it.asDomainModel().asDatabaseModel())
                        nextPageKey = base.response.nextKey
                    }
                MediatorResult.Success(base.response.nextKey == null)
            }.onErrorResumeNext {
                if (it is IOException || it is HttpException) {
                    Single.just<MediatorResult>(MediatorResult.Error(it))
                }
                Single.error(it)
            }

    private fun formatDateAndAvatar(posts: List<PostProperty>): List<PostProperty> {
        val mutableList = mutableListOf<PostProperty>()
        posts.map { post ->
            val formattedDate = DateTime(post.date * 1000, DateTimeZone.forOffsetHours(
                OFFSET_HOURS))
            val copy = post.copy(formattedDate = formattedDate, avatarImgUrl = "")
            mutableList.add(copy)
        }
        return mutableList
    }

    private fun attachPostToGroup(
        posts: List<PostProperty>,
        groups: List<GroupProperty>,
        users: List<Profile>
    ): List<PostProperty> {
        val mutableList = mutableListOf<PostProperty>()
        posts.forEach { post ->
            var groupname: String
            var avatarImg: String
            groups.find { group ->
                group.id == -post.sourceId
            }?.let {
                groupname = it.name
                avatarImg = it.photo100
                mutableList.add(
                    post.copy(
                        groupName = groupname,
                        avatarImgUrl = avatarImg
                    )
                )
            } ?: users.find {user ->
                user.id == post.sourceId
            }?.let {
                groupname = (it.firstName + " " + it.lastName)
                avatarImg = it.photo50
                mutableList.add(
                    post.copy(
                        groupName = groupname,
                        avatarImgUrl = avatarImg
                    )
                )
            }?: kotlin.run {
                mutableList.add(
                    post.copy(
                        groupName = "",
                        avatarImgUrl = ""
                    )
                )
            }
        }
        return mutableList
    }

    private fun prepareFavorites(posts: List<PostProperty>): List<PostProperty> {
        return posts.map { post ->
            val isFavorite = post.isFavorite || post.likes.userLikes == 1
            favoritePosts[post.postId] = isFavorite
            if (isFavorite) {
                post.copy(isFavorite = isFavorite)
            } else {
                post
            }
        }
    }

    private fun setPhotoLink(list: List<PostProperty>): List<PostProperty> {
        val mutableList = mutableListOf<PostProperty>()
        list.forEach { post ->
            post.attachments?.forEach { attachment ->
                attachment.photo?.let { photo ->
                    val photoLink =
                        if (!photo.sizes.isNullOrEmpty()) photo.sizes[4].url else null
                    val copy = post.copy(photoLink = photoLink)
                    mutableList.add(copy)
                }
            }
        }
        return mutableList
    }

    companion object {
        const val OFFSET_HOURS = 4
        const val NEXT_PAGE_KEY = "nextPageKey"
    }

}