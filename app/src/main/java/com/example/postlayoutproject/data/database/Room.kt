package com.example.postlayoutproject.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.postlayoutproject.data.database.dao.PostDao

const val DATABASE_NAME = "posts"

@Database(entities = [DatabasePost::class], version = 3, exportSchema = false)
abstract class PostsDatabase : RoomDatabase() {
    abstract val postDao: PostDao
}