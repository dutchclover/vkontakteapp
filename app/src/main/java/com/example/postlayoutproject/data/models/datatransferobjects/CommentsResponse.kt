package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class CommentsResponse (
    @SerializedName("count") val count : Int,
    @SerializedName("items") val items : List<Comment>,
    @SerializedName("profiles") val profiles : List<Profile>,
    @SerializedName("groups") val groups : List<GroupProperty>,
    @SerializedName("current_level_count") val currentLevelCount : Int,
    @SerializedName("can_post") val canPost : Boolean,
    @SerializedName("show_reply_button") val showReplyButton : Boolean
)
