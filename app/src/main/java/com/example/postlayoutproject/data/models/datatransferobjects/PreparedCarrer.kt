package com.example.postlayoutproject.data.models.datatransferobjects

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PreparedCareer(
    val city: String?,
    val country: String?,
    val company: String?,
    val from: Int?,
    val until: Int?,
    val position: String?
): Parcelable
