package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class UserPostsResponse(
    @SerializedName("count") val count : Int,
    @SerializedName("items") val items : List<UserPostProperty>,
    @SerializedName("profiles") val profiles : List<Profile>,
    @SerializedName("groups") val groups : List<GroupProperty>
)