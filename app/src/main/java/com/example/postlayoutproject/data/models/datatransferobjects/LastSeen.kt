package com.example.postlayoutproject.data.models.datatransferobjects

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LastSeen (
    @SerializedName("platform") val platform : Int,
    @SerializedName("time") val time : Long
): Parcelable