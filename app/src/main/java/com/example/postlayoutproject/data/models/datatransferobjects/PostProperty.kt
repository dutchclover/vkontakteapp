package com.example.postlayoutproject.data.models.datatransferobjects

import android.os.Parcelable
import com.example.postlayoutproject.data.models.Post
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.util.*

@Parcelize
data class PostProperty(
    @SerializedName("source_id") val sourceId: Int,
    @SerializedName("date") val date: Long,
    val formattedDate: DateTime,
    val avatarImgUrl: String,
    val groupName: String?,
    val photoLink: String?,
    @SerializedName("post_type") val postType: String,
    @SerializedName("text") val text: String,
    @SerializedName("attachments") val attachments: List<Attachments>?,
    @SerializedName("post_source") val postSource: PostSource,
    @SerializedName("comments") val comments: Comments,
    @SerializedName("likes") val likes: Likes,
    @SerializedName("reposts") val reposts: Reposts,
    @SerializedName("is_favorite") val isFavorite: Boolean,
    @SerializedName("post_id") val postId: Int,
    @SerializedName("type") val type: String
) : Parcelable

fun List<PostProperty>.asDomainModel(): List<Post> {
    return map {
        Post(
            sourceId = it.sourceId,
            date = it.date,
            formattedDate = it.formattedDate.toString(dateFormat),
            avatarImgUrl = it.avatarImgUrl,
            groupName = it.groupName,
            photoLink = it.photoLink,
            text = it.text,
            comments = it.comments.count,
            likes = it.likes.count,
            reposts = it.reposts.count,
            isFavorite = it.isFavorite,
            postId = it.postId,
            type = it.type
        )
    }
}

private val dateFormat = DateTimeFormat.forPattern("dd MMMM YYYY HH:mm").withLocale(Locale("ru"))