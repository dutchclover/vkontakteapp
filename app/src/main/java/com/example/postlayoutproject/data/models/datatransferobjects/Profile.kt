package com.example.postlayoutproject.data.models.datatransferobjects

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Profile (
    @SerializedName("first_name") val firstName : String,
    @SerializedName("id") val id : Int,
    @SerializedName("last_name") val lastName : String,
    @SerializedName("can_access_closed") val canAccessClosed : Boolean,
    @SerializedName("is_closed") val isClosed : Boolean,
    @SerializedName("domain") val domain : String,
    @SerializedName("bdate") val bdate : String?,
    @SerializedName("city") val city : City?,
    @SerializedName("home_town") val homeTown : String?,
    @SerializedName("country") val country : Country?,
    @SerializedName("photo_max") val photo : String?,
    @SerializedName("photo_50") val photo50 : String,
    @SerializedName("photo_100") val photo100 : String,
    @SerializedName("about") val about : String?,
    @SerializedName("last_seen") val lastSeen : LastSeen,
    @SerializedName("career") val career : List<Career?>,
    @SerializedName("followers_count") val followersCount : Int?,
    @SerializedName("university") val university : Int,
    @SerializedName("university_name") val universityName : String,
    @SerializedName("faculty") val faculty : Int,
    @SerializedName("faculty_name") val facultyName : String,
    @SerializedName("graduation") val graduation : Int?,
    @SerializedName("education_form") val educationForm : String?,
    @SerializedName("education_status") val educationStatus : String,
    var preparedCareers: MutableList<PreparedCareer>
) : Parcelable