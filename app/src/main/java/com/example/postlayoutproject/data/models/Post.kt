package com.example.postlayoutproject.data.models

import android.os.Parcelable
import com.example.postlayoutproject.data.database.DatabasePost
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Post(
    @SerializedName("source_id") val sourceId: Int,
    @SerializedName("date") val date: Long,
    val formattedDate: String,
    val avatarImgUrl: String,
    val groupName: String?,
    val photoLink: String?,
    @SerializedName("text") val text : String,
    @SerializedName("comments") val comments : Int,
    @SerializedName("likes") val likes : Int,
    @SerializedName("reposts") val reposts : Int,
    @SerializedName("is_favorite") val isFavorite : Boolean,
    @SerializedName("post_id") val postId : Int,
    @SerializedName("type") val type : String
): Parcelable

fun List<Post>.asDatabaseModel(): Array<DatabasePost> {
    return map {
        DatabasePost(
            sourceId = it.sourceId,
            date = it.date,
            formattedDate = it.formattedDate,
            avatarImgUrl = it.avatarImgUrl,
            groupName = it.groupName,
            photoLink = it.photoLink,
            text = it.text,
            comments = it.comments,
            likes = it.likes,
            reposts = it.reposts,
            isFavorite = it.isFavorite,
            postId = it.postId,
            type = it.type
        )
    }.toTypedArray()
}