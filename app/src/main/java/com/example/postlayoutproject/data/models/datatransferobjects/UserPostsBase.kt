package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class UserPostsBase(
    @SerializedName("response") val response : UserPostsResponse
)