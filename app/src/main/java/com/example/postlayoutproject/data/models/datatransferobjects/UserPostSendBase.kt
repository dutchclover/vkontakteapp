package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class UserPostSendBase(
    @SerializedName("response") val response : UserPostSendResponse
)