package com.example.postlayoutproject.data.models.datatransferobjects

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Likes (
    @SerializedName("count") val count : Int,
    @SerializedName("user_likes") val userLikes : Int,
) : Parcelable

