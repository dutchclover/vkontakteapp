package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class GroupProperty (
    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("type") val type : String,
    @SerializedName("photo_50") val photo50 : String,
    @SerializedName("photo_100") val photo100 : String,
    @SerializedName("photo_200") val photo200 : String
)