package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class CommentSendBase (
    @SerializedName("response") val response : CommentSendResponse
)