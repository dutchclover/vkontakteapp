package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class UserPostSendResponse (
    @SerializedName("post_id") val postId : Int
)