package com.example.postlayoutproject.data.network

import com.example.postlayoutproject.data.models.datatransferobjects.*
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface VkApiService {

    @GET("newsfeed.get?filters=post&return_banned=0&v=5.124")
    fun getPostsAsync(
        @Query("access_token") accessToken: String?,
        @Query("start_from") startFrom: String? = null,
        @Query("count") loadSize: Int = 0,
    ): Single<Base>

    @GET("likes.add?v=5.124&type=post")
    fun likePost(
        @Query("item_id") postId: Int,
        @Query("owner_id") ownerId: Int,
        @Query("access_token") accessToken: String?
    ): Single<BaseLikes>

    @GET("likes.delete?v=5.124&type=post")
    fun dislikePost(
        @Query("item_id") postId: Int,
        @Query("owner_id") ownerId: Int,
        @Query("access_token") accessToken: String?
    ): Single<BaseLikes>

    @GET("newsfeed.ignoreItem?v=5.124&type=wall")
    fun ignorePost(
        @Query("item_id") postId: Int,
        @Query("owner_id") ownerId: Int,
        @Query("access_token") accessToken: String?
    ): Observable<Int>

    @GET("wall.getComments?extended=1&fields=first_name,last_name,name,photo_50&offset=0&need_likes=0&preview_length=0&v=5.124")
    fun getComments(
        @Query("access_token") accessToken: String?,
        @Query("owner_id") ownerId: Int,
        @Query("post_id") postId: Int,
        @Query("start_comment_id") startFrom: Int? = null,
        @Query("count") loadSize: Int? = 30,
        @Query("sort") sort: String = "desc",
    ): Single<BaseComments>

    @POST("wall.createComment?&v=5.124")
    fun createComment(
        @Query("message") message: String,
        @Query("access_token") accessToken: String?,
        @Query("owner_id") ownerId: Int,
        @Query("post_id") postId: Int,
        @Query("guid") guid: String,
    ): Single<CommentSendBase>

    @GET("users.get?&fields=domain,first_name,last_name,photo_max,home_town,about,bdate,city,country,career,education,followers_count,last_seen&v=5.124")
    fun getProfile(
        @Query("access_token") accessToken: String?,
    ): Single<BaseProfile>

    @POST("wall.post?&v=5.124")
    fun createPost(
        @Query("message") message: String,
        @Query("access_token") accessToken: String?,
        @Query("guid") guid: String,
    ): Single<UserPostSendBase>

    @GET("database.getCountriesById?&v=5.124")
    fun getCountry(
        @Query("country_ids") countryIds: String,
        @Query("access_token") accessToken: String?,
    ): Single<CountryBase>

    @GET("database.getCitiesById?&v=5.124")
    fun getCity(
        @Query("city_ids") cityIds: String,
        @Query("access_token") accessToken: String?,
    ): Single<CityBase>

    @GET("groups.getById?&v=5.124")
    fun getCompany(
        @Query("group_ids") groupIds: String,
        @Query("access_token") accessToken: String?,
    ): Single<GroupBase>

    @GET("wall.get?filter=owner&extended=1&v=5.124&")
    fun getUserPosts(
        @Query("access_token") accessToken: String?,
        @Query("offset") startFrom: Int? = null,
        @Query("count") loadSize: Int = 0,
    ): Single<UserPostsBase>
}