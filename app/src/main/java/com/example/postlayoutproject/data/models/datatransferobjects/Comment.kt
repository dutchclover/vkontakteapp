package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class Comment (
    @SerializedName("id") val id : Int,
    @SerializedName("from_id") val fromId : Int,
    val firstName: String?,
    val lastName: String?,
    val name: String?,
    val photo: String?,
    val formattedDate : String,
    @SerializedName("date") val date : Long,
    @SerializedName("text") val text : String?,
)