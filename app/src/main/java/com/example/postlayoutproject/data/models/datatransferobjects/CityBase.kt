package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class CityBase(
    @SerializedName("response") val response : List<CityResponse>
)