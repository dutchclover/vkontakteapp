package com.example.postlayoutproject.data.models.datatransferobjects

import com.google.gson.annotations.SerializedName

data class GroupBase(
    @SerializedName("response") val response : List<GroupProperty>
)