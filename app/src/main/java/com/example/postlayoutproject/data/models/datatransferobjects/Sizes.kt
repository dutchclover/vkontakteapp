package com.example.postlayoutproject.data.models.datatransferobjects

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Sizes (
    @SerializedName("height") val height : Int,
    @SerializedName("url") val url : String,
    @SerializedName("type") val type : String,
    @SerializedName("width") val width : Int
) : Parcelable