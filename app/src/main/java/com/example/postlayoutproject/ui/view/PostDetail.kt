package com.example.postlayoutproject.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.marginBottom
import androidx.core.view.marginEnd
import androidx.core.view.marginStart
import androidx.core.view.marginTop
import com.example.postlayoutproject.R
import kotlinx.android.synthetic.main.post_detail.view.*

class PostDetail @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ViewGroup(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.post_detail, this, true)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val desiredWidth = MeasureSpec.getSize(widthMeasureSpec)
        var height = 0

        measureChildWithMargins(avatarImg, widthMeasureSpec, 0, heightMeasureSpec, height)

        measureChildWithMargins(label, widthMeasureSpec- avatarImg.measuredWidth - avatarImg.marginStart - avatarImg.marginEnd, 0, heightMeasureSpec, height)
        height += label.measuredHeight + label.marginTop + label.marginBottom

        postImg.scaleType = ImageView.ScaleType.FIT_XY
        postImg.adjustViewBounds = true

        measureChildWithMargins(postImg, widthMeasureSpec, 0, heightMeasureSpec, height)
        height += postImg.measuredHeight + postImg.marginTop + postImg.marginBottom

        measureChildWithMargins(postText, widthMeasureSpec, 0, heightMeasureSpec, height)
        height += postText.measuredHeight + postText.marginTop + postText.marginBottom


        measureChildWithMargins(likeBtn, widthMeasureSpec, 0, heightMeasureSpec, height)

        measureChildWithMargins(likeCounter, widthMeasureSpec, 0, heightMeasureSpec, height)

        measureChildWithMargins(commentBtn, widthMeasureSpec, 0, heightMeasureSpec, height)

        measureChildWithMargins(commentCounter, widthMeasureSpec, 0, heightMeasureSpec, height)

        measureChildWithMargins(shareBtn, widthMeasureSpec, 0, heightMeasureSpec, height)

        measureChildWithMargins(shareCounter, widthMeasureSpec, 0, heightMeasureSpec, height)
        height += likeCounter.measuredHeight + likeCounter.marginTop + likeCounter.marginBottom

        setMeasuredDimension(desiredWidth, resolveSize(height, heightMeasureSpec))
    }

    override fun onLayout(change: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        var currentLeft = 0 + paddingLeft
        var currentTop = 0 + paddingTop

        avatarImg.layout(
            currentLeft + avatarImg.marginStart,
            currentTop + avatarImg.marginTop,
            currentLeft + avatarImg.measuredWidth + avatarImg.marginEnd,
            currentTop + avatarImg.measuredHeight + avatarImg.marginBottom
        )
        currentLeft += avatarImg.measuredWidth + avatarImg.marginStart + avatarImg.marginEnd

        label.layout(
            currentLeft + label.marginStart,
            currentTop + label.marginTop,
            currentLeft + label.marginStart + label.measuredWidth,
            label.measuredHeight + label.marginTop
        )
        currentTop += avatarImg.measuredHeight + avatarImg.marginTop + avatarImg.marginBottom
        currentLeft = 0 + paddingLeft

        postImg.layout(
            currentLeft + postImg.marginStart,
            currentTop + postImg.marginTop,
            currentLeft + postImg.measuredWidth + postImg.marginEnd,
            currentTop + postImg.measuredHeight + postImg.marginBottom
        )

        currentTop += postImg.measuredHeight + postImg.marginTop + postImg.marginBottom

        postText.layout(
            currentLeft + postText.marginStart,
            currentTop + postText.marginTop,
            currentLeft + postText.measuredWidth + postText.marginStart,
            currentTop + postText.measuredHeight + postText.marginBottom
        )
        currentTop += postText.measuredHeight + postText.marginTop + postText.marginBottom

        likeBtn.layout(
            currentLeft + likeBtn.marginStart,
            currentTop + likeBtn.marginTop,
            currentLeft + likeBtn.measuredWidth + likeBtn.marginEnd,
            currentTop + likeBtn.measuredHeight + likeBtn.likeBtn.marginBottom
        )
        currentLeft += likeBtn.measuredWidth + likeBtn.marginStart + likeBtn.marginEnd

        likeCounter.layout(
            currentLeft + likeCounter.marginStart,
            currentTop + likeCounter.marginTop,
            currentLeft + likeCounter.measuredWidth + likeCounter.marginStart,
            currentTop + likeCounter.measuredHeight + likeCounter.marginBottom
        )
        currentLeft += likeCounter.measuredWidth + likeCounter.marginStart + likeCounter.marginEnd

        commentBtn.layout(
            currentLeft + commentBtn.marginStart,
            currentTop + commentBtn.marginTop,
            currentLeft + commentBtn.measuredWidth + commentBtn.marginStart,
            currentTop + commentBtn.measuredHeight + commentBtn.marginBottom
        )
        currentLeft += commentBtn.measuredWidth + commentBtn.marginStart + commentBtn.marginEnd

        commentCounter.layout(
            currentLeft + commentCounter.marginStart,
            currentTop + commentCounter.marginTop,
            currentLeft + commentCounter.measuredWidth + commentCounter.marginStart,
            currentTop + commentCounter.measuredHeight + commentCounter.marginBottom
        )
        currentLeft += commentCounter.measuredWidth + commentCounter.marginStart + commentCounter.marginEnd

        shareBtn.layout(
            currentLeft + shareBtn.marginStart,
            currentTop + shareBtn.marginTop,
            currentLeft + shareBtn.measuredWidth + shareBtn.marginStart,
            currentTop + shareBtn.measuredHeight + shareBtn.marginBottom
        )
        currentLeft += shareBtn.measuredWidth + shareBtn.marginStart + shareBtn.marginEnd

        shareCounter.layout(
            currentLeft + shareCounter.marginStart,
            currentTop + shareCounter.marginTop,
            currentLeft + shareCounter.measuredWidth + shareCounter.marginStart,
            currentTop + shareCounter.measuredHeight + shareCounter.marginBottom
        )
    }

    override fun generateLayoutParams(attrs: AttributeSet?) = MarginLayoutParams(context, attrs)

    override fun generateDefaultLayoutParams(): LayoutParams =
        MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
}