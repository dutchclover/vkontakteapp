package com.example.postlayoutproject.ui.holders


import android.content.res.ColorStateList
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.core.widget.ImageViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.postlayoutproject.R
import com.example.postlayoutproject.data.models.Post
import kotlinx.android.synthetic.main.post_image.view.*

abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    open fun bind(post: Post): Unit = with(itemView) {

        val imgUri = post.avatarImgUrl.toUri().buildUpon().scheme(SCHEME).build()
        Glide.with(context)
            .load(imgUri)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image)
            )
            .into(avatarImg)

        label.text = post.groupName

        postDate.text = post.formattedDate

        postText.text = post.text

        Glide.with(context)
            .load(post.photoLink)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image)
            )
            .into(postImg)

        colorFavorite(likeBtn, post.isFavorite)

        likeCounter.text = post.likes.toString()

        commentCounter.text = post.comments.toString()

        shareCounter.text = post.reposts.toString()
    }

    private fun colorFavorite(view: View, isFavorite: Boolean) {
        val color = if (isFavorite) {
            android.R.color.holo_red_dark
        } else {
            android.R.color.darker_gray
        }

        ImageViewCompat.setImageTintList(
            view as ImageView,
            ColorStateList.valueOf(
                ContextCompat.getColor(
                    view.context,
                    color
                )
            )
        )
    }

    companion object {
        const val SCHEME = "https"
    }

}