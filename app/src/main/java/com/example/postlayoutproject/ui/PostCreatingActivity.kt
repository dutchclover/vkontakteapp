package com.example.postlayoutproject.ui

import android.accounts.NetworkErrorException
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.ViewModelProvider
import com.example.postlayoutproject.R
import com.example.postlayoutproject.ui.state.Action
import com.example.postlayoutproject.ui.state.Effect
import com.example.postlayoutproject.ui.state.State
import com.example.postlayoutproject.viewmodels.CreatePostViewModel
import kotlinx.android.synthetic.main.activity_creating_post.*
import javax.inject.Inject

class PostCreatingActivity : MviActivity<State, Effect, Action, CreatePostViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    override val viewModel by viewModels<CreatePostViewModel> { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_creating_post)

        closeBtn.setOnClickListener {
            finish()
        }
        doneBtn.setOnClickListener {
            if (postMessage.text.isNotEmpty()) {
                viewModel.process(Action.CreatePost(postMessage.text.toString()))
            }
        }
    }

    override fun renderViewState(viewState: State) {
        if (viewState.error == NetworkErrorException() ) {
            Toast.makeText(this, NETWORK_ERROR_MESSAGE +  "${viewState.error}", Toast.LENGTH_SHORT).show()
        }
        else if (viewState.error != null) {
            Toast.makeText(this, OTHER_ERROR_MESSAGE + "${viewState.error}", Toast.LENGTH_SHORT).show()
        }
        if (viewState.dataLoaded) {
            Toast.makeText(this, SUCCESS_MESSAGE, Toast.LENGTH_SHORT).show()
            finish()
        }
    }

    override fun renderViewEffect(viewEffect: Effect) {
    }

    companion object {
        const val SUCCESS_MESSAGE = "Пост успешно опубликован"
        const val NETWORK_ERROR_MESSAGE = "Отсутствует интернет соединение, пожалуйста, попробуйте позже:"
        const val OTHER_ERROR_MESSAGE = "Произошла непредвиденная ошибка, пожалуйста, попробуйте позже:"

    }
}