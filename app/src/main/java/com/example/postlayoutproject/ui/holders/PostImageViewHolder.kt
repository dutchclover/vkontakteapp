package com.example.postlayoutproject.ui.holders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.example.postlayoutproject.R
import com.example.postlayoutproject.data.models.Post
import kotlinx.android.synthetic.main.post_image.view.*


class PostImageViewHolder(itemView: View) : BaseViewHolder(itemView) {

    override fun bind(post: Post): Unit = with(itemView) {
        super.bind(post)
        postImg.isVisible = true
    }

    companion object {
        fun create(parent: ViewGroup): PostImageViewHolder {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_post, parent, false)
            return PostImageViewHolder(view)
        }
    }
}


