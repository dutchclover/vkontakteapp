package com.example.postlayoutproject.ui.adapters

import androidx.recyclerview.widget.DiffUtil
import com.example.postlayoutproject.data.models.UserPost

object UserPostsComparator :  DiffUtil.ItemCallback<UserPost>() {
    override fun areItemsTheSame(oldItem: UserPost, newItem: UserPost): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: UserPost, newItem: UserPost) = oldItem == newItem
}