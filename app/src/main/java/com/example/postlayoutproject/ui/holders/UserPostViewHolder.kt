package com.example.postlayoutproject.ui.holders

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.core.widget.ImageViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.postlayoutproject.R
import com.example.postlayoutproject.data.models.UserPost
import kotlinx.android.synthetic.main.post_image.view.*

class UserPostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(userPost: UserPost): Unit = with(itemView) {
        repostedAvatarImg.isGone = true
        repostedLabel.isGone = true
        repostedDate.isGone = true

        val imgUri = userPost.avatarImgUrl.toUri().buildUpon().scheme("https").build()
        Glide.with(context)
            .load(imgUri)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image)
            )
            .into(avatarImg)

        label.text = userPost.groupOrUserName

        postDate.text = userPost.formattedDate

        if (userPost.repostedText.isNotEmpty() && userPost.text.isNotEmpty()) {
            userPostText.isVisible = true
            userPostText.text = userPost.text
        }

        if (userPost.repostedAvatarImgUrl.isNotEmpty()) {
            repostedAvatarImg.isVisible = true
            val repostedImgUri =
                userPost.repostedAvatarImgUrl.toUri().buildUpon().scheme("https").build()
            Glide.with(context)
                .load(repostedImgUri)
                .apply(
                    RequestOptions()
                        .placeholder(R.drawable.loading_animation)
                        .error(R.drawable.ic_broken_image)
                )
                .into(repostedAvatarImg)
        }

        if (!userPost.repostedGroupOrUserName.isNullOrEmpty()) {
            repostedLabel.isVisible = true
            repostedLabel.text = userPost.repostedGroupOrUserName
        }

        if (!userPost.repostedFormattedDate.isNullOrEmpty()) {
            repostedDate.isVisible = true
            repostedDate.text = userPost.repostedFormattedDate
        }

        postText.text = when {
            userPost.repostedText.isNotEmpty() -> userPost.repostedText
            userPost.copyHistory.isNullOrEmpty() -> userPost.text
            else -> ""
        }

        postImg.isVisible = !userPost.photoLink.isNullOrEmpty()
        Glide.with(context)
            .load(userPost.photoLink)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image)
            )
            .into(postImg)


        colorFavorite(likeBtn, userPost.isFavorite)

        likeCounter.text = userPost.likes.count.toString()

        commentCounter.text = userPost.comments.count.toString()

        shareCounter.text = userPost.reposts.count.toString()
    }

    private fun colorFavorite(view: View, isFavorite: Boolean) {
        val color = if (isFavorite) {
            android.R.color.holo_red_dark
        } else {
            android.R.color.darker_gray
        }

        ImageViewCompat.setImageTintList(
            view as ImageView,
            ColorStateList.valueOf(
                ContextCompat.getColor(
                    view.context,
                    color
                )
            )
        )
    }

    companion object {
        fun create(parent: ViewGroup): UserPostViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_post, parent, false)
            return UserPostViewHolder(view)
        }
    }
}