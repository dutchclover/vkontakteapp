package com.example.postlayoutproject.ui.adapters

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.postlayoutproject.data.models.Post
import com.example.postlayoutproject.ui.holders.BaseViewHolder
import com.example.postlayoutproject.ui.holders.PostImageViewHolder
import com.example.postlayoutproject.ui.holders.PostTextViewHolder
import com.example.postlayoutproject.ui.holders.SeparatorItemViewHolder
import com.example.postlayoutproject.viewmodels.PostViewModel.UiModel
import kotlinx.android.synthetic.main.item_separator.view.*


class PostAdapter(diffCallback: DiffUtil.ItemCallback<UiModel>, val adapterOnClick: (Int) -> Unit) :
    PagingDataAdapter<UiModel, RecyclerView.ViewHolder>(diffCallback) {

    companion object {
        const val ITEM_VIEW_TYPE_IMAGE = 0
        const val ITEM_VIEW_TYPE_TEXT = 1
        const val SEPARATOR_VIEW_TYPE = 2
        const val UNKNOWN_TYPE = "Unknown viewType"
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)
        return when {
            item is UiModel.PostItem && hasPhotos(item.post) -> ITEM_VIEW_TYPE_IMAGE
            item is UiModel.PostItem && !hasPhotos(item.post) -> ITEM_VIEW_TYPE_TEXT
            item is UiModel.SeparatorItem -> SEPARATOR_VIEW_TYPE
            else -> throw UnsupportedOperationException(UNKNOWN_TYPE)
        }
    }

    private fun hasPhotos(post: Post): Boolean {
        return !post.photoLink.isNullOrEmpty()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): RecyclerView.ViewHolder {
        val holder = when (viewType) {
            ITEM_VIEW_TYPE_IMAGE -> PostImageViewHolder.create(parent)
            ITEM_VIEW_TYPE_TEXT -> PostTextViewHolder.create(parent)
            else -> SeparatorItemViewHolder.create(parent)
        }
        holder.itemView.setOnClickListener {
            adapterOnClick(holder.absoluteAdapterPosition)
        }
        return holder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        item.let {
            when (item) {
                is UiModel.PostItem -> {
                    val viewHolder = holder as BaseViewHolder
                    viewHolder.bind(post = item.post)
                }
                is UiModel.SeparatorItem -> {
                    val viewHolder = holder as SeparatorItemViewHolder
                    viewHolder.itemView.separator_description.text = item.description
                }
            }
        }
    }

    fun getCurrentPostId(position: Int) = this.getPost(position)?.postId

    fun getCurrentPostOwner(position: Int) = this.getPost(position)?.sourceId

    fun isCurrentPostFavorite(position: Int) = this.getPost(position)?.isFavorite

    fun getPost(position: Int): Post? {
        val item = getItem(position)
        return if (item is UiModel.PostItem) item.post
        else null
    }
}