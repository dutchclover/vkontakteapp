package com.example.postlayoutproject.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import com.example.postlayoutproject.TAG
import com.example.postlayoutproject.viewmodels.MviViewModel
import dagger.android.support.DaggerFragment

abstract class MviFragment<STATE, EFFECT, ACTION, ViewModel : MviViewModel<STATE, EFFECT, ACTION>> :
    DaggerFragment() {

    abstract val viewModel: ViewModel

    private val viewStateObserver = Observer<STATE> {
        Log.d(TAG, OBSERVED_VIEWSTATE + "$it")
        renderViewState(it)
    }

    private val viewEffectObserver = Observer<EFFECT> {
        Log.d(TAG, OBSERVED_VIEWEFFECT + "$it")
        renderViewEffect(it)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.viewStates().observe(viewLifecycleOwner, viewStateObserver)
        viewModel.viewEffects().observe(viewLifecycleOwner, viewEffectObserver)
        super.onViewCreated(view, savedInstanceState)
    }

    abstract fun renderViewState(viewState: STATE)

    abstract fun renderViewEffect(viewEffect: EFFECT)

    companion object {
        const val OBSERVED_VIEWSTATE = "observed viewState:"
        const val OBSERVED_VIEWEFFECT = "observed viewEffect:"
    }
}