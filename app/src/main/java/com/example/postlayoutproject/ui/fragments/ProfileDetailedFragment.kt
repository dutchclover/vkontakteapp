package com.example.postlayoutproject.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.example.postlayoutproject.R
import com.example.postlayoutproject.data.models.datatransferobjects.PreparedCareer
import com.example.postlayoutproject.data.models.datatransferobjects.Profile
import com.example.postlayoutproject.ui.state.Action
import com.example.postlayoutproject.ui.state.Effect
import com.example.postlayoutproject.ui.state.State
import com.example.postlayoutproject.viewmodels.ProfileViewModel
import kotlinx.android.synthetic.main.fragment_profile_details.*
import javax.inject.Inject

class ProfileDetailedFragment : MviFragment<State, Effect, Action, ProfileViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    override val viewModel by viewModels<ProfileViewModel> { viewModelFactory }
    private lateinit var profile: Profile

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if ((requireArguments().getParcelable(PROFILE) as Profile?) != null) {
            profile = (requireArguments().getParcelable(PROFILE) as Profile?)!!
            bindProfile(profile)
        }
    }

    private fun bindProfile(profile: Profile?) {

        profile?.bdate?.let {
            birthdayText.text = profile.bdate
        } ?: run {
            birthdayImg.isVisible = false
            birthdayLabel.isVisible = false
            birthdayText.isVisible = false
        }

        profile?.city?.let {
            cityText.text = profile.city.title
        } ?: run {
            cityImg.isVisible = false
            cityLabel.isVisible = false
            cityText.isVisible = false
        }

        val work = profile?.preparedCareers?.lastOrNull()
        work?.let {
            careerText.text = it.company
        } ?: run {
            careerText.isVisible = false
            careerImg.isVisible = false
            careerLabel.isVisible = false
        }

        profile?.homeTown?.let {
            homeTownText.text = profile.homeTown
        } ?: run {
            homeTownLabel.isVisible = false
            homeTownText.isVisible = false
        }


        if (!profile?.universityName.isNullOrEmpty()) {
            if (profile?.graduation == null || profile.graduation == 0) {
                universityText.text = profile?.universityName
            } else {
                universityText.text =
                    (profile.universityName + "'" + profile.graduation)
            }
            if (!profile?.facultyName.isNullOrEmpty()) {
                facultyText.text = profile?.facultyName
                facultyText.isGone = true
            } else {
            }
        } else {
            universityLabel.isGone= true
            educationInfo.isGone= true
            universityText.isGone= true
            universityLabel.isGone= true
            facultyText.isGone = true
        }

        if (!profile?.career.isNullOrEmpty()) {
            careerInfo.isVisible = true
            detailedCareerText.text = profile?.preparedCareers?.let { showCarrierInfo(it) }
        } else {
            detailedCareerText.isVisible = false
            careerInfo.isVisible = false
        }
    }

    override fun renderViewEffect(viewEffect: Effect) {
        when (viewEffect) {
            is Effect.ShowProfileDetails -> bindProfile(viewEffect.profile)
        }
    }

    private fun showCarrierInfo(careers: List<PreparedCareer>): String {
        val builder = StringBuilder()
        careers.forEach {
            builder.append(it.company)
                .append("\n")
                .append(if (it.country != null && it.country != RUSSIA) "${it.country}, " else "")
                .append(if (it.city != null) "${it.city} " else " ")
                .append(if (it.from != null) FROM + "${it.from}" else "")
                .append(if (it.until != null) UNTIL + "${it.until}" else "")
                .append("\n")
                .append(it.position ?: "")
                .append("\n")
                .append("\n")
        }
        return builder.toString()
    }

    override fun renderViewState(viewState: State) {
    }

    companion object {
        const val PROFILE = "Profile"
        const val RUSSIA = "Россия"
        const val FROM = "c"
        const val UNTIL = "по"
    }
}