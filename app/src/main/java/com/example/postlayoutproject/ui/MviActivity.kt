package com.example.postlayoutproject.ui

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import com.example.postlayoutproject.TAG
import com.example.postlayoutproject.viewmodels.MviViewModel
import dagger.android.support.DaggerAppCompatActivity

abstract class MviActivity<STATE, EFFECT, EVENT, ViewModel : MviViewModel<STATE, EFFECT, EVENT>> :
    DaggerAppCompatActivity() {

    abstract val viewModel: ViewModel

    private val viewStateObserver = Observer<STATE> {
        Log.d(TAG, OBSERVED_VIEWSTATE + "$it")
        renderViewState(it)
    }

    private val viewEffectObserver = Observer<EFFECT> {
        Log.d(TAG, OBSERVED_VIEWEFFECT + "$it")
        renderViewEffect(it)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.viewStates().observe(this, viewStateObserver)
        viewModel.viewEffects().observe(this, viewEffectObserver)
    }

    abstract fun renderViewState(viewState: STATE)

    abstract fun renderViewEffect(viewEffect: EFFECT)

    companion object {
        const val OBSERVED_VIEWSTATE = "observed viewState:"
        const val OBSERVED_VIEWEFFECT = "observed viewEffect:"
    }
}