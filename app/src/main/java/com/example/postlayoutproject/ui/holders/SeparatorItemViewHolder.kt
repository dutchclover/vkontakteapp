package com.example.postlayoutproject.ui.holders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.postlayoutproject.R

class SeparatorItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    companion object {
        fun create(parent: ViewGroup): SeparatorItemViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_separator, parent, false)
            return SeparatorItemViewHolder(view)
        }
    }
}