package com.example.postlayoutproject.ui

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.ViewModelProvider
import com.example.postlayoutproject.R
import com.example.postlayoutproject.ui.fragments.NavigationFragment
import com.example.postlayoutproject.ui.state.Action
import com.example.postlayoutproject.ui.state.Effect
import com.example.postlayoutproject.ui.state.State
import com.example.postlayoutproject.viewmodels.MainActivityViewModel
import com.vk.api.sdk.VK
import com.vk.api.sdk.VKTokenExpiredHandler
import com.vk.api.sdk.auth.VKAccessToken
import com.vk.api.sdk.auth.VKAuthCallback
import com.vk.api.sdk.auth.VKScope
import javax.inject.Inject

class MainActivity : MviActivity<State, Effect, Action, MainActivityViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    override val viewModel by viewModels<MainActivityViewModel> { viewModelFactory }

    private val tokenTracker = object : VKTokenExpiredHandler {
        override fun onTokenExpired() {
            viewModel.process(Action.Logging)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    private fun loginVK() {
        VK.login(this@MainActivity, arrayListOf(VKScope.WALL, VKScope.FRIENDS))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val callback = object : VKAuthCallback {
            override fun onLogin(token: VKAccessToken) {
                VK.saveAccessToken(this@MainActivity, token.userId, token.accessToken, token.secret)
                VK.addTokenExpiredHandler(tokenTracker)
                val accessToken = token.accessToken
                println("accessToken is $accessToken")
                viewModel.process(Action.Logged(accessToken))
            }

            override fun onLoginFailed(errorCode: Int) {
                showVKLoginErrorAlert()
                viewModel.process(Action.Logging)
            }
        }
        if (data == null || !VK.onActivityResult(requestCode, resultCode, data, callback)) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun showVKLoginErrorAlert() {
        Toast.makeText(this, LOGIN_ERROR_MESSAGE, Toast.LENGTH_LONG)
            .show()
    }

    private fun showNavigationFragment() {
        if (supportFragmentManager.findFragmentByTag(NAVIGATION_FRAGMENT) == null) {
            supportFragmentManager.beginTransaction()
                .setCustomAnimations(
                    R.anim.slide_in,
                    R.anim.fade_out,
                    R.anim.fade_in,
                    R.anim.slide_out
                )
                .replace(
                    R.id.fragment_container,
                    NavigationFragment(), NAVIGATION_FRAGMENT
                )
                .commitAllowingStateLoss()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        VK.removeTokenExpiredHandler(tokenTracker)
    }

    companion object {
        const val NAVIGATION_FRAGMENT = "Navigation"
        const val LOGIN_ERROR_MESSAGE = "Ошибка авторизации вконтакте, попробуйте еще раз"
    }

    override fun renderViewState(viewState: State) {
        when {
            viewState.isEmptyState -> {
                viewModel.process(Action.Logging)
            }
            viewState.isLogging -> {
                loginVK()
            }
            viewState.isLogged -> {
                showNavigationFragment()
            }
        }
    }

    override fun renderViewEffect(viewEffect: Effect) {
    }
}
