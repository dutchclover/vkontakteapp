package com.example.postlayoutproject.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.marginBottom
import androidx.core.view.marginEnd
import androidx.core.view.marginStart
import androidx.core.view.marginTop
import com.example.postlayoutproject.R
import kotlinx.android.synthetic.main.post_image.view.*


class PostLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ViewGroup(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.post_image, this, true)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val desiredWidth = MeasureSpec.getSize(widthMeasureSpec)
        var height = 0

        measureChildWithMargins(avatarImg, widthMeasureSpec, 0, heightMeasureSpec, height)

        measureChildWithMargins(
            label,
            widthMeasureSpec - avatarImg.measuredWidth - avatarImg.marginStart - avatarImg.marginEnd,
            0,
            heightMeasureSpec,
            height
        )
        height += label.measuredHeight + label.marginTop + label.marginBottom

        measureChildWithMargins(postDate, widthMeasureSpec, 0, heightMeasureSpec, height)
        height += postDate.measuredHeight + postDate.marginTop + postDate.marginBottom

        if (userPostText.visibility != GONE) {
            measureChildWithMargins(userPostText, widthMeasureSpec, 0, heightMeasureSpec, height)
            height += userPostText.measuredHeight + userPostText.marginTop + userPostText.marginBottom
        }

        if (repostedAvatarImg.visibility != GONE) {
            measureChildWithMargins(
                repostedAvatarImg,
                widthMeasureSpec,
                0,
                heightMeasureSpec,
                height
            )
        }

        if (repostedLabel.visibility != GONE) {
            measureChildWithMargins(
                repostedLabel,
                widthMeasureSpec - repostedAvatarImg.measuredWidth - repostedAvatarImg.marginStart - repostedAvatarImg.marginEnd,
                0,
                heightMeasureSpec,
                height
            )
            height += repostedLabel.measuredHeight + repostedLabel.marginTop + repostedLabel.marginBottom
        }

        if (repostedDate.visibility != GONE) {
            measureChildWithMargins(repostedDate, widthMeasureSpec, 0, heightMeasureSpec, height)
            height += repostedDate.measuredHeight + repostedDate.marginTop + repostedDate.marginBottom
        }

        measureChildWithMargins(postText, widthMeasureSpec, 0, heightMeasureSpec, height)
        height += postText.measuredHeight + postText.marginTop + postText.marginBottom

        if (postImg.visibility != GONE) {
            postImg.scaleType = ImageView.ScaleType.FIT_XY
            postImg.adjustViewBounds = true

            measureChildWithMargins(postImg, widthMeasureSpec, 0, heightMeasureSpec, height)
            height += postImg.measuredHeight + postImg.marginTop + postImg.marginBottom
        }

        measureChildWithMargins(likeBtn, widthMeasureSpec, 0, heightMeasureSpec, height)

        measureChildWithMargins(likeCounter, widthMeasureSpec, 0, heightMeasureSpec, height)

        measureChildWithMargins(commentBtn, widthMeasureSpec, 0, heightMeasureSpec, height)

        measureChildWithMargins(commentCounter, widthMeasureSpec, 0, heightMeasureSpec, height)

        measureChildWithMargins(shareBtn, widthMeasureSpec, 0, heightMeasureSpec, height)

        measureChildWithMargins(shareCounter, widthMeasureSpec, 0, heightMeasureSpec, height)
        height += likeCounter.measuredHeight + likeCounter.marginTop + likeCounter.marginBottom

        setMeasuredDimension(desiredWidth, resolveSize(height, heightMeasureSpec))
    }

    override fun onLayout(change: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        var currentLeft = 0 + paddingLeft
        var currentTop = 0 + paddingTop

        avatarImg.layout(
            currentLeft + avatarImg.marginStart,
            currentTop + avatarImg.marginTop,
            currentLeft + avatarImg.measuredWidth + avatarImg.marginEnd,
            currentTop + avatarImg.measuredHeight + avatarImg.marginBottom
        )
        currentLeft += avatarImg.measuredWidth + avatarImg.marginStart + avatarImg.marginEnd

        label.layout(
            currentLeft + label.marginStart,
            currentTop + label.marginTop,
            currentLeft + label.marginStart + label.measuredWidth,
            label.measuredHeight + label.marginTop
        )
        currentTop += label.measuredHeight + label.marginTop + label.marginBottom

        postDate.layout(
            currentLeft + postDate.marginStart,
            currentTop + postDate.marginTop,
            currentLeft + postDate.marginStart + postDate.measuredWidth,
            currentTop + postDate.measuredHeight + postDate.marginTop
        )
        currentTop += postDate.measuredHeight + postDate.marginBottom + postDate.marginTop
        currentLeft = 0 + paddingLeft

        if (userPostText.visibility != GONE) {
            repostedDate.layout(
                currentLeft + userPostText.marginStart,
                currentTop + userPostText.marginTop,
                currentLeft + userPostText.marginStart + userPostText.measuredWidth,
                currentTop + userPostText.measuredHeight + userPostText.marginTop
            )
        }

        currentTop += userPostText.measuredHeight + userPostText.marginTop + userPostText.marginBottom

        if (repostedAvatarImg.visibility != GONE) {
            repostedAvatarImg.layout(
                currentLeft + repostedAvatarImg.marginStart,
                currentTop + repostedAvatarImg.marginTop,
                currentLeft + repostedAvatarImg.measuredWidth + repostedAvatarImg.marginEnd,
                currentTop + repostedAvatarImg.measuredHeight + repostedAvatarImg.marginBottom
            )

            currentLeft += repostedAvatarImg.measuredWidth + repostedAvatarImg.marginStart + repostedAvatarImg.marginEnd
        }

        if (repostedLabel.visibility != GONE) {
            repostedLabel.layout(
                currentLeft + repostedLabel.marginStart,
                currentTop + repostedLabel.marginTop,
                currentLeft + repostedLabel.marginStart + repostedLabel.measuredWidth,
                currentTop + repostedLabel.measuredHeight + repostedLabel.marginTop
            )
            currentTop += repostedLabel.measuredHeight + repostedLabel.marginTop + repostedLabel.marginBottom
        }

        if (repostedDate.visibility != GONE) {
            repostedDate.layout(
                currentLeft + repostedDate.marginStart,
                currentTop + repostedDate.marginTop,
                currentLeft + repostedDate.marginStart + repostedDate.measuredWidth,
                currentTop + repostedDate.measuredHeight + repostedDate.marginTop
            )

            currentTop += repostedDate.measuredHeight + repostedDate.marginBottom + repostedDate.marginTop
            currentLeft = 0 + paddingLeft
        }

        postText.layout(
            currentLeft + postText.marginStart,
            currentTop + postText.marginTop,
            currentLeft + postText.measuredWidth + postText.marginStart,
            currentTop + postText.measuredHeight + postText.marginBottom
        )
        currentTop += postText.measuredHeight + postText.marginTop + postText.marginBottom

        if (postImg.visibility != GONE) {
            postImg.layout(
                currentLeft + postImg.marginStart,
                currentTop + postImg.marginTop,
                currentLeft + postImg.measuredWidth + postImg.marginEnd,
                currentTop + postImg.measuredHeight + postImg.marginBottom
            )
            currentTop += postImg.measuredHeight + postImg.marginTop + postImg.marginBottom
        }

        likeBtn.layout(
            currentLeft + likeBtn.marginStart,
            currentTop + likeBtn.marginTop,
            currentLeft + likeBtn.measuredWidth + likeBtn.marginEnd,
            currentTop + likeBtn.measuredHeight + likeBtn.likeBtn.marginBottom
        )
        currentLeft += likeBtn.measuredWidth + likeBtn.marginStart + likeBtn.marginEnd

        likeCounter.layout(
            currentLeft + likeCounter.marginStart,
            currentTop + likeCounter.marginTop,
            currentLeft + likeCounter.measuredWidth + likeCounter.marginStart,
            currentTop + likeCounter.measuredHeight + likeCounter.marginBottom
        )
        currentLeft += likeCounter.measuredWidth + likeCounter.marginStart + likeCounter.marginEnd

        commentBtn.layout(
            currentLeft + commentBtn.marginStart,
            currentTop + commentBtn.marginTop,
            currentLeft + commentBtn.measuredWidth + commentBtn.marginStart,
            currentTop + commentBtn.measuredHeight + commentBtn.marginBottom
        )
        currentLeft += commentBtn.measuredWidth + commentBtn.marginStart + commentBtn.marginEnd

        commentCounter.layout(
            currentLeft + commentCounter.marginStart,
            currentTop + commentCounter.marginTop,
            currentLeft + commentCounter.measuredWidth + commentCounter.marginStart,
            currentTop + commentCounter.measuredHeight + commentCounter.marginBottom
        )
        currentLeft += commentCounter.measuredWidth + commentCounter.marginStart + commentCounter.marginEnd

        shareBtn.layout(
            currentLeft + shareBtn.marginStart,
            currentTop + shareBtn.marginTop,
            currentLeft + shareBtn.measuredWidth + shareBtn.marginStart,
            currentTop + shareBtn.measuredHeight + shareBtn.marginBottom
        )
        currentLeft += shareBtn.measuredWidth + shareBtn.marginStart + shareBtn.marginEnd

        shareCounter.layout(
            currentLeft + shareCounter.marginStart,
            currentTop + shareCounter.marginTop,
            currentLeft + shareCounter.measuredWidth + shareCounter.marginStart,
            currentTop + shareCounter.measuredHeight + shareCounter.marginBottom
        )
    }

    override fun generateLayoutParams(attrs: AttributeSet?) = MarginLayoutParams(context, attrs)

    override fun generateDefaultLayoutParams(): LayoutParams =
        MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
}








