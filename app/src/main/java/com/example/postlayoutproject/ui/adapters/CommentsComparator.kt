package com.example.postlayoutproject.ui.adapters

import androidx.recyclerview.widget.DiffUtil
import com.example.postlayoutproject.data.models.datatransferobjects.Comment

object CommentsComparator : DiffUtil.ItemCallback<Comment>() {
    override fun areItemsTheSame(oldItem: Comment, newItem: Comment): Boolean {
        return oldItem.id == newItem.id
    }
    override fun areContentsTheSame(oldItem: Comment, newItem: Comment) = oldItem == newItem
}

