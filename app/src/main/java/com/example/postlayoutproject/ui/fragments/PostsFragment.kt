package com.example.postlayoutproject.ui.fragments

import android.os.Bundle
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import com.example.postlayoutproject.R
import com.example.postlayoutproject.data.models.Post
import com.example.postlayoutproject.ui.state.Action
import com.example.postlayoutproject.ui.state.Effect
import com.example.postlayoutproject.viewmodels.PostViewModel
import javax.inject.Inject

class PostsFragment : BasePostsFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    override val viewModel by activityViewModels<PostViewModel> { viewModelFactory }

    override fun onPostLeftSwipe(postId: Int, ownerId: Int, isFavorite: Boolean) {
        viewModel.process(Action.DeletePost(postId, ownerId))
    }

    override fun renderViewEffect(viewEffect: Effect) {
        when (viewEffect) {
            is Effect.ShowDetails -> showDetailFragment(viewEffect.post)
        }
    }

    private fun showDetailFragment(post: Post) {
        val fragment = PostDetailFragment()
        val bundle = Bundle()
        bundle.putParcelable(SELECTED_POST, post)
        fragment.arguments = bundle
        requireActivity().supportFragmentManager.beginTransaction()
            .setCustomAnimations(
                R.anim.slide_in,
                R.anim.fade_out,
                R.anim.fade_in,
                R.anim.slide_out
            )
            .replace(R.id.fragment_container, fragment, DETAIL_FRAGMENT)
            .addToBackStack(null)
            .commit()
    }

    companion object {
        const val DETAIL_FRAGMENT = "Detail"
        const val SELECTED_POST = "selectedPost"
    }
}