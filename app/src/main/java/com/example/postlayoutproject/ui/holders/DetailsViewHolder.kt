package com.example.postlayoutproject.ui.holders

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.core.view.ViewCompat
import androidx.core.widget.ImageViewCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.postlayoutproject.R
import com.example.postlayoutproject.data.models.Post
import com.example.postlayoutproject.ui.fragments.PostDetailFragment
import kotlinx.android.synthetic.main.post_image.view.*

class DetailsViewHolder(itemView: View) : BaseViewHolder(itemView) {

    override fun bind(post: Post): Unit = with(itemView) {

        post.avatarImgUrl.let {
            val imgUri = it.toUri().buildUpon().scheme(PostDetailFragment.SCHEME_HTTPS).build()
            Glide.with(context)
                .load(imgUri)
                .apply(
                    RequestOptions()
                        .placeholder(R.drawable.loading_animation)
                        .error(R.drawable.ic_broken_image)
                )
                .into(avatarImg)
        }

        label.text = post.groupName

        ViewCompat.setTransitionName(postImg, post.postId.toString())

        Glide.with(context)
            .load(post.photoLink)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image)
            )
            .into(postImg)

        postText.text = post.text

        val color = if (post.isFavorite) {
            android.R.color.holo_red_dark
        } else {
            android.R.color.darker_gray
        }
        ImageViewCompat.setImageTintList(
            likeBtn,
            ColorStateList.valueOf(
                ContextCompat.getColor(
                    context,
                    color
                )
            )
        )

        likeCounter.text = post.likes.toString()

        commentCounter.text = post.comments.toString()

        shareCounter.text = post.reposts.toString()
    }

    companion object {
        fun create(parent: ViewGroup): DetailsViewHolder {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_detail, parent, false)
            return DetailsViewHolder(view)
        }
    }
}