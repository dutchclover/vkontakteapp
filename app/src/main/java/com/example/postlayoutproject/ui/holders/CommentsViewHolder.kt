package com.example.postlayoutproject.ui.holders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.postlayoutproject.R
import com.example.postlayoutproject.data.models.datatransferobjects.Comment
import kotlinx.android.synthetic.main.item_comments.view.*

class CommentsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(comment: Comment): Unit = with(itemView) {
        val imgUri = comment.photo?.toUri()?.buildUpon()?.scheme("https")?.build()
        Glide.with(context)
            .load(imgUri)
            .apply(
                RequestOptions()
                    .circleCrop()
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image)
            )
            .into(userImg)

        userName.text = comment.firstName ?: comment.name
        userSurname.text = comment.lastName ?: ""
        commentsText.text = comment.text
        commentsDate.text = comment.formattedDate
    }

    companion object {
        fun create(parent: ViewGroup): CommentsViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_comments, parent, false)
            return CommentsViewHolder(view)
        }
    }
}
