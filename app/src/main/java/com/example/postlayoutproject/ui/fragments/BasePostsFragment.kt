package com.example.postlayoutproject.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.postlayoutproject.R
import com.example.postlayoutproject.ui.adapters.PostAdapter
import com.example.postlayoutproject.ui.adapters.PostComparator
import com.example.postlayoutproject.ui.adapters.PostLoadingAdapter
import com.example.postlayoutproject.ui.state.Action
import com.example.postlayoutproject.ui.state.Effect
import com.example.postlayoutproject.ui.state.State
import com.example.postlayoutproject.viewmodels.PostViewModel
import kotlinx.android.synthetic.main.fragment_posts.*

abstract class BasePostsFragment : MviFragment<State, Effect, Action, PostViewModel>() {

    private lateinit var manager: LinearLayoutManager
    lateinit var postAdapter: PostAdapter
    private var shouldScrollToTop = false
    override val viewModel: PostViewModel by activityViewModels()

    private val adapterDataObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            if (shouldScrollToTop) {
                postsList.scrollToPosition(0)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_posts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        manager = LinearLayoutManager(activity)
        shouldScrollToTop = false
        postAdapter = PostAdapter(PostComparator) { item ->
            val post = postAdapter.getPost(item)
            post?.let { Action.PostClicked(it) }?.let { viewModel.process(it) }
        }

        postAdapter.registerAdapterDataObserver(adapterDataObserver)

        postsList.apply {
            layoutManager = manager
            adapter = postAdapter.withLoadStateFooter(
                footer = PostLoadingAdapter { postAdapter.retry() })
        }

        val itemTouchHelperCallback = object :
            ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                if (direction == ItemTouchHelper.LEFT) {
                    postAdapter.getCurrentPostId(viewHolder.absoluteAdapterPosition)?.let {
                        postAdapter.isCurrentPostFavorite(viewHolder.absoluteAdapterPosition)?.let { it1 ->
                            onPostLeftSwipe(
                                it,
                                postAdapter.getCurrentPostOwner(viewHolder.absoluteAdapterPosition)!!,
                                it1
                            )
                        }
                    }
                } else {
                    postAdapter.isCurrentPostFavorite(viewHolder.absoluteAdapterPosition)?.let {
                        postAdapter.getCurrentPostId(viewHolder.absoluteAdapterPosition)?.let { it1 ->
                            Action.ChangeFavorite(
                                it1,
                                postAdapter.getCurrentPostOwner(viewHolder.absoluteAdapterPosition)!!,
                                it
                            )
                        }
                    }?.let {
                        viewModel.process(
                            it
                        )
                    }
                }
            }
        }

        val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
        itemTouchHelper.attachToRecyclerView(postsList)

        swipeRefresh.setOnRefreshListener {
            shouldScrollToTop = true
            viewModel.process(Action.FetchData)
        }
    }

    abstract fun onPostLeftSwipe(postId: Int, ownerId: Int, isFavorite: Boolean)

    override fun onDestroyView() {
        super.onDestroyView()
        postAdapter.unregisterAdapterDataObserver(adapterDataObserver)
    }

    override fun renderViewState(viewState: State) {
        if (viewState.isEmptyState) {
            shouldScrollToTop = true
            viewModel.process(Action.FetchData)
        }
        if (viewState.isLoading) {
            renderLoadingView()
        }
        if (viewState.error != null) {
            renderErrorView()
        }
        if (viewState.dataLoaded) {
            renderLoadedView(viewState)
        }
    }

    private fun renderLoadingView() {
        shimmerLayout.startShimmer()
        shimmerLayout.isVisible = true
        postsList.isVisible = false
        errorImg.isVisible = false
        errorTxt.isVisible = false
        swipeRefresh.isRefreshing = true
    }

    private fun renderErrorView() {
        shimmerLayout.stopShimmer()
        shimmerLayout.isVisible = false
        postsList.isVisible = false
        errorImg.isVisible = true
        errorTxt.isVisible = true
        swipeRefresh.isRefreshing = false
    }

    private fun renderLoadedView(viewState: State) {
        shimmerLayout.stopShimmer()
        shimmerLayout.isVisible = false
        postsList.isVisible = true
        errorImg.isVisible = false
        errorTxt.isVisible = false
        postAdapter.submitData(viewLifecycleOwner.lifecycle, viewState.post)
        swipeRefresh.isRefreshing = false
    }
}