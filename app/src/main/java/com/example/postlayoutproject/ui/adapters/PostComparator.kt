package com.example.postlayoutproject.ui.adapters

import androidx.recyclerview.widget.DiffUtil
import com.example.postlayoutproject.viewmodels.PostViewModel.UiModel

object PostComparator : DiffUtil.ItemCallback<UiModel>() {
    override fun areItemsTheSame(oldItem: UiModel, newItem: UiModel): Boolean {
        val isSamePostItem = oldItem is UiModel.PostItem
                && newItem is UiModel.PostItem
                && oldItem.post.postId == newItem.post.postId
                && oldItem.post.comments == newItem.post.comments

        val isSameSeparatorItem = oldItem is UiModel.SeparatorItem
                && newItem is UiModel.SeparatorItem
                && oldItem.description == newItem.description

        return isSamePostItem || isSameSeparatorItem
    }

    override fun areContentsTheSame(oldItem: UiModel, newItem: UiModel) = oldItem == newItem
}