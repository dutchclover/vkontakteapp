package com.example.postlayoutproject.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import com.example.postlayoutproject.R
import com.example.postlayoutproject.ui.state.Action
import com.example.postlayoutproject.ui.state.Effect
import com.example.postlayoutproject.ui.state.State
import com.example.postlayoutproject.viewmodels.NavigationViewModel
import kotlinx.android.synthetic.main.fragment_navigation.*
import javax.inject.Inject
import kotlin.math.round

class NavigationFragment : MviFragment<State, Effect, Action, NavigationViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    override val viewModel by activityViewModels<NavigationViewModel> { viewModelFactory }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_navigation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (savedInstanceState == null &&
            childFragmentManager.findFragmentByTag(FavoritePostsFragment::class.java.name) == null
        ) {
            viewModel.process(Action.ShowPosts)
        }

        bottomNav.setOnNavigationItemSelectedListener { item ->
            if (bottomNav.selectedItemId == item.itemId) {
                return@setOnNavigationItemSelectedListener true
            }
            when (item.itemId) {
                R.id.action_show_posts -> viewModel.process(Action.ShowPosts)
                R.id.action_show_favorite_posts -> viewModel.process(Action.ShowFavoritePosts)
                R.id.action_show_profile -> viewModel.process(Action.ShowProfile)
            }
            true
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        if (childFragmentManager.findFragmentByTag(fragment::class.java.name) == null) {
            childFragmentManager.beginTransaction()
                .setCustomAnimations(
                    R.anim.fade_in,
                    R.anim.fade_out,
                    R.anim.fade_in,
                    R.anim.fade_out
                )
                .replace(
                    R.id.child_fragment_container,
                    fragment,
                    fragment::class.java.name
                )
                .commit()
        }
    }

    override fun renderViewState(viewState: State) {
        val itemMenuFavorites = bottomNav.menu.findItem(R.id.action_show_favorite_posts)
        if (viewState.hasFavorites) {
            itemMenuFavorites.isVisible = true
        } else if (!viewState.hasFavorites) {
            bottomNav.selectedItemId = R.id.action_show_posts
            itemMenuFavorites.isVisible = false
        }
    }

    override fun renderViewEffect(viewEffect: Effect) {
        when (viewEffect) {
            is Effect.ShowPosts -> replaceFragment(PostsFragment())
            is Effect.ShowFavoritePosts -> replaceFragment(FavoritePostsFragment())
            is Effect.ShowProfile -> replaceFragment(ProfileFragment())
        }
    }

    fun race(v1:Int, v2:Int, g:Int): IntArray {
        if (v1 >= v2) return intArrayOf()

        val diff = v2 -v1
        val sec  = (g/diff).toDouble() * 3600
        val hours = round(sec).toInt()
        val minutes = round(sec/60).toInt()
        val seconds = (round(sec/60) % 60).toInt()
        return  intArrayOf(hours,minutes,seconds)
    }
}