package com.example.postlayoutproject.ui.adapters

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.postlayoutproject.data.models.Post
import com.example.postlayoutproject.data.models.datatransferobjects.Comment
import com.example.postlayoutproject.ui.holders.CommentsViewHolder
import com.example.postlayoutproject.ui.holders.DetailsViewHolder

class DetailAdapter(
    private val post: Post,
    diffCallback: DiffUtil.ItemCallback<Comment>,
    private val detailClickListener: DetailClickListener
) : PagingDataAdapter<Comment, RecyclerView.ViewHolder>(diffCallback) {

    interface DetailClickListener {
        fun onPostClick(post: Post)
    }

    override fun getItemViewType(position: Int): Int =
        if (position == itemCount - 1)
            ITEM_VIEW_TYPE_POST
        else
            ITEM_VIEW_TYPE_COMMENT

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM_VIEW_TYPE_POST -> DetailsViewHolder.create(parent)
            ITEM_VIEW_TYPE_COMMENT -> CommentsViewHolder.create(parent)
            else -> throw ClassCastException(UNKNOWN_TYPE + "$viewType")
        }
    }

    override fun getItemCount(): Int = super.getItemCount() + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position == itemCount - 1) {
            val viewHolder = holder as DetailsViewHolder
            viewHolder.itemView.setOnLongClickListener {
                detailClickListener.onPostClick(post)
                return@setOnLongClickListener true
            }
            viewHolder.bind(post = post)
        } else {
            getItem(position)?.let { comment ->
                val viewHolder = holder as CommentsViewHolder
                viewHolder.bind(comment = comment)
            }
        }
    }

    companion object {
        const val ITEM_VIEW_TYPE_POST = 0
        const val ITEM_VIEW_TYPE_COMMENT = 1
        const val UNKNOWN_TYPE = "Unknown viewType"
    }
}