package com.example.postlayoutproject.ui.fragments

import android.Manifest
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.*
import android.widget.ImageView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.postlayoutproject.R
import com.example.postlayoutproject.data.models.Post
import com.example.postlayoutproject.obtainBitmap
import com.example.postlayoutproject.ui.adapters.CommentsComparator
import com.example.postlayoutproject.ui.adapters.DetailAdapter
import com.example.postlayoutproject.ui.adapters.PostLoadingAdapter
import com.example.postlayoutproject.ui.state.Action
import com.example.postlayoutproject.ui.state.Effect
import com.example.postlayoutproject.ui.state.State
import com.example.postlayoutproject.viewmodels.DetailViewModel
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.post_detail.*
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import javax.inject.Inject

class PostDetailFragment : MviFragment<State, Effect, Action, DetailViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    override val viewModel by viewModels<DetailViewModel> { viewModelFactory }
    private var actionMode: ActionMode? = null
    private lateinit var detailAdapter: DetailAdapter
    private lateinit var post: Post

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        post = (requireArguments().getParcelable(SELECTED_POST_KEY) as Post?)!!
        viewModel.process(Action.PostClicked(post))

        detailAdapter = DetailAdapter(post,
            CommentsComparator, object : DetailAdapter.DetailClickListener {
                override fun onPostClick(post: Post) {
                    actionMode = activity?.startActionMode(actionModeCallback)
                }
            })

        detailsList.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, true)
            adapter = detailAdapter.withLoadStateFooter(
                footer = PostLoadingAdapter { detailAdapter.retry() })
        }

        sendBtn.setOnClickListener {
            commentToSend.text.let {
                viewModel.process(Action.SendComment(it.toString(), post.sourceId, post.postId))
                it.clear()
            }
        }

        swipeRefresh.setOnRefreshListener {
            viewModel.process(Action.PostClicked(post))
        }
    }

    private val actionModeCallback = object : ActionMode.Callback {

        override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
            val inflater: MenuInflater = mode.menuInflater
            inflater.inflate(R.menu.context_menu, menu)
            return true
        }

        override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean = false

        override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
            return when (item.itemId) {
                R.id.action_share -> {
                    if (checkAndRequestPermission(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            REQUEST_PERMISSION_STORAGE_FOR_SHARE_IMAGE
                        )
                    ) {
                        shareImage()
                    }
                    mode.finish()
                    true
                }
                R.id.action_save -> {
                    if (checkAndRequestPermission(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            REQUEST_PERMISSION_STORAGE_FOR_SAVE_IMAGE
                        )
                    ) {
                        saveImage()
                    }
                    mode.finish()
                    true
                }
                else -> false
            }
        }

        override fun onDestroyActionMode(mode: ActionMode) {
            actionMode = null
        }
    }

    fun shareImage() {
        val image = postImg as ImageView
        image.obtainBitmap()?.let {
            viewModel.process(Action.ShareImage(it))
        } ?: showSharingError()
    }

    private fun shareImage(uri: Uri) = Intent().apply {
        action = Intent.ACTION_SEND
        putExtra(Intent.EXTRA_STREAM, uri)
        type = SHARE_INTENT_TYPE
        startActivity(Intent.createChooser(this, SHARE_ACTIVITY_TITLE))
    }

    private fun showSharingError() =
        Toast.makeText(context, getString(R.string.sharing_failed), Toast.LENGTH_SHORT).show()

    private fun saveImage() {

        val drawable = postImg.drawable as BitmapDrawable
        val bitmap = drawable.bitmap

        if (android.os.Build.VERSION.SDK_INT >= SDK_VERSION_29) {
            val values = contentValues()
            values.put(MediaStore.Images.Media.RELATIVE_PATH, PICTURES_PATH)
            values.put(MediaStore.Images.Media.IS_PENDING, true)

            val uri: Uri? = requireContext().contentResolver.insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                values
            )
            if (uri != null) {
                saveImageToStream(bitmap, requireContext().contentResolver.openOutputStream(uri))
                values.put(MediaStore.Images.Media.IS_PENDING, false)
                requireContext().contentResolver.update(uri, values, null, null)
            }
        } else {
            val directory = File(Environment.getExternalStorageDirectory().toString())
            if (!directory.exists()) {
                directory.mkdirs()
            }
            val fileName = System.currentTimeMillis().toString() + IMAGE_EXTENSION_PNG
            val file = File(directory, fileName)
            saveImageToStream(bitmap, FileOutputStream(file))
            val values = contentValues()
            values.put(MediaStore.Images.Media.DATA, file.absolutePath)
            requireContext().contentResolver.insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                values
            )
        }
    }

    private fun contentValues(): ContentValues {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.MIME_TYPE, SHARE_INTENT_TYPE)
        values.put(MediaStore.Images.Media.DATE_ADDED, System.currentTimeMillis() / 1000)
        return values
    }

    private fun saveImageToStream(bitmap: Bitmap, outputStream: OutputStream?) {
        if (outputStream != null) {
            try {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
                outputStream.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun checkAndRequestPermission(permission: String, requestCode: Int): Boolean {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                permission
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(
                    permission
                ), requestCode
            )
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_PERMISSION_STORAGE_FOR_SAVE_IMAGE) {
            if (permissions[0] == Manifest.permission.WRITE_EXTERNAL_STORAGE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                saveImage()
            }
        } else if (requestCode == REQUEST_PERMISSION_STORAGE_FOR_SHARE_IMAGE) {
            if (permissions[0] == Manifest.permission.WRITE_EXTERNAL_STORAGE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                shareImage()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        val args = Bundle()
        args.putString(FRAGMENT_NAME_KEY, FRAGMENT_NAME)
    }

    override fun renderViewState(viewState: State) {
        if (viewState.isLoading) {
            Toast.makeText(requireContext(), LOADING, Toast.LENGTH_SHORT).show()
            swipeRefresh.isRefreshing = true
        }
        if (viewState.error != null) {
            renderErrorView(viewState.error)
            swipeRefresh.isRefreshing = false
        }
        if (viewState.dataLoaded) {
            swipeRefresh.isRefreshing = false
            detailAdapter.submitData(viewLifecycleOwner.lifecycle, viewState.comment)
        }
    }

    private fun renderErrorView(e: Throwable) {
        Toast.makeText(requireContext(), ERROR + "$e", Toast.LENGTH_LONG).show()
    }

    override fun renderViewEffect(viewEffect: Effect) {
        when (viewEffect) {
            is Effect.SharedImageUri -> viewEffect.uri?.let {
                shareImage(it)
            } ?: showSharingError()
        }
    }

    companion object {
        const val LOADING = "Loading"
        const val ERROR = "Something went wrong:"
        const val FRAGMENT_NAME_KEY = "Key"
        const val SELECTED_POST_KEY = "selectedPost"
        const val FRAGMENT_NAME = "DetailFragment"
        const val AUTHORITY = "com.example.postlayoutproject"
        const val SHARE_ACTIVITY_TITLE = "Share Image"
        const val SHARE_INTENT_TYPE = "image/png"
        const val SHARED_IMAGE_TITLE = "shared_image_"
        const val IMAGE_EXTENSION_PNG = ".png"
        const val PICTURES_PATH = "Pictures"
        const val SCHEME_HTTPS = "https"
        const val SDK_VERSION_29 = 29
        const val REQUEST_PERMISSION_STORAGE_FOR_SAVE_IMAGE = 1
        const val REQUEST_PERMISSION_STORAGE_FOR_SHARE_IMAGE = 2
    }
}