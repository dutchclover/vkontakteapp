package com.example.postlayoutproject.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.postlayoutproject.R
import com.example.postlayoutproject.data.models.datatransferobjects.Profile
import com.example.postlayoutproject.ui.PostCreatingActivity
import com.example.postlayoutproject.ui.adapters.ProfileAdapter
import com.example.postlayoutproject.ui.adapters.UserPostsComparator
import com.example.postlayoutproject.ui.state.Action
import com.example.postlayoutproject.ui.state.Effect
import com.example.postlayoutproject.ui.state.State
import com.example.postlayoutproject.viewmodels.ProfileViewModel
import kotlinx.android.synthetic.main.fragment_profile.*
import javax.inject.Inject


class ProfileFragment : MviFragment<State, Effect, Action, ProfileViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    override val viewModel by viewModels<ProfileViewModel> { viewModelFactory }
    lateinit var profileAdapter: ProfileAdapter
    private var scrollToTop = false

    private val adapterDataObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            if (scrollToTop) {
                scrollToTop = false
                profileList.scrollToPosition(0)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        profileAdapter = ProfileAdapter(UserPostsComparator, object : ProfileAdapter.ProfileClickListener {
            override fun onDetailsClick(profile: Profile) {
                viewModel.process(Action.ShowDetailedProfile(profile))
            }
        }).also {
            it.registerAdapterDataObserver(adapterDataObserver)
        }

        profileList.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            adapter = profileAdapter
            itemAnimator = null
        }

        fab.setOnClickListener {
            val intent = Intent(activity, PostCreatingActivity::class.java)
            startActivity(intent)
        }

        swipeRefresh.setOnRefreshListener {
            viewModel.process(Action.ShowProfile)
        }
    }

    override fun renderViewState(viewState: State) = when {
        viewState.profileLoaded -> {
            swipeRefresh.isRefreshing = false
            viewState.profile?.let { profileAdapter.setProfile(it) }
            viewModel.process(Action.ShowUserPosts)
        }
        viewState.isLoadingUserPosts -> {
            Toast.makeText(requireContext(), LOADING, Toast.LENGTH_SHORT).show()
        }
        viewState.isEmptyState -> {
            viewModel.process(Action.ShowProfile)
        }
        viewState.userPostsLoaded -> {
            swipeRefresh.isRefreshing = false
            profileAdapter.submitData(viewLifecycleOwner.lifecycle, viewState.userPost)
        }
        else -> {
            println("Not handled state $viewState") //throw IllegalStateException(viewState.toString())
        }
    }

    companion object {
        const val PROFILE_DETAILS_FRAGMENT = "Profile details"
        const val PROFILE = "Profile"
        const val LOADING = "Loading"
    }

    private fun showProfileDetailedFragment(profile: Profile) {
        val fragment = ProfileDetailedFragment()
        val bundle = Bundle()
        bundle.putParcelable(PROFILE, profile)
        fragment.arguments = bundle
        if (requireActivity().supportFragmentManager.findFragmentByTag(PROFILE_DETAILS_FRAGMENT) == null) {
            requireActivity().supportFragmentManager.beginTransaction()
                .setCustomAnimations(
                    R.anim.slide_top,
                    R.anim.slide_bottom,
                    R.anim.slide_top,
                    R.anim.slide_bottom
                )
                .replace(
                    R.id.rootNavigation,
                    fragment
                )
                .addToBackStack(ProfileDetailedFragment::class.java.name)
                .commit()
        }
    }

    override fun renderViewEffect(viewEffect: Effect) {
        when (viewEffect) {
            is Effect.ShowProfileDetails -> showProfileDetailedFragment(viewEffect.profile)
            is Effect.ScrollToTop -> scrollToTop = true
        }
    }
}