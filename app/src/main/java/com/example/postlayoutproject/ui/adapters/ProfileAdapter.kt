package com.example.postlayoutproject.ui.adapters

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.postlayoutproject.data.models.UserPost
import com.example.postlayoutproject.data.models.datatransferobjects.Profile
import com.example.postlayoutproject.ui.holders.ProfileViewHolder
import com.example.postlayoutproject.ui.holders.UserPostViewHolder

class ProfileAdapter(
    diffCallback: DiffUtil.ItemCallback<UserPost>,
    private val profileClickListener: ProfileClickListener
) : PagingDataAdapter<UserPost, RecyclerView.ViewHolder>(diffCallback) {

    interface ProfileClickListener {
        fun onDetailsClick(profile: Profile)
    }

    private lateinit var mProfile: Profile

    fun setProfile(profile: Profile) {
        if (!this::mProfile.isInitialized) {
            mProfile = profile
            notifyItemInserted(0)
        }
    }

    override fun getItemViewType(position: Int): Int =
        if (position == 0)
            ITEM_VIEW_TYPE_PROFILE
        else
            ITEM_VIEW_TYPE_USER_POST

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM_VIEW_TYPE_PROFILE -> ProfileViewHolder.create(profileClickListener, parent)
            ITEM_VIEW_TYPE_USER_POST -> UserPostViewHolder.create(parent)
            else -> throw ClassCastException(UNKNOWN_TYPE + "$viewType")
        }
    }

    override fun getItemCount(): Int = if (this::mProfile.isInitialized)
        super.getItemCount() + 1
    else
        super.getItemCount()

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        println(getItemId(position))
        if (position == 0) {
            val viewHolder = holder as ProfileViewHolder
            if (this::mProfile.isInitialized) viewHolder.bind(profile = mProfile)
        } else {
            val viewHolder = holder as UserPostViewHolder
            getItem(position - 1)?.let { viewHolder.bind(userPost = it) }
        }
    }

    companion object {
        const val ITEM_VIEW_TYPE_PROFILE = 0
        const val ITEM_VIEW_TYPE_USER_POST = 1
        const val UNKNOWN_TYPE = "Unknown viewType"
    }
}