package com.example.postlayoutproject.ui.holders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.postlayoutproject.R
import com.example.postlayoutproject.data.models.datatransferobjects.Profile
import com.example.postlayoutproject.ui.adapters.ProfileAdapter
import kotlinx.android.synthetic.main.item_comments.view.userImg
import kotlinx.android.synthetic.main.item_comments.view.userName
import kotlinx.android.synthetic.main.item_comments.view.userSurname
import kotlinx.android.synthetic.main.item_profile.view.*
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter


class ProfileViewHolder(
    val profileClickListener: ProfileAdapter.ProfileClickListener,
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    fun bind(profile: Profile) = with(itemView) {
        val imgUri =
            profile.photo?.toUri()?.buildUpon()?.scheme("https")?.build()
        Glide.with(context)
            .load(imgUri)
            .apply(
                RequestOptions()
                    .circleCrop()
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image)
            )
            .into(userImg)

        userName.text = profile.firstName
        userSurname.text = profile.lastName
        userDomain.text = profile.domain
        aboutText.text = profile.about ?: ""
        lastSeenText.text =
            formatDate(
                DateTime(
                    profile.lastSeen.time.times(1000),
                    DateTimeZone.getDefault()
                )
            )
        birthdayText.text = profile.bdate ?: ""
        countryText.text = profile.country?.title ?: ""
        cityText.text = profile.city?.title ?: ""


        if (profile.universityName.isNotEmpty()) {
            if (profile.graduation == null || profile.graduation == 0) {
                educationText.text = profile.universityName
            } else educationText.text =
                (profile.universityName + "'" + profile.graduation)
        } else {
            educationLabel.isVisible = false
            educationImg.isVisible = false
            educationText.isVisible = false
        }

        val work = profile.preparedCareers.lastOrNull()
        work?.let {
            careerText.text = it.company ?: ""
        } ?: run {
            careerText.isVisible = false
            careerImg.isVisible = false
            careerLabel.isVisible = false
        }

        profile.followersCount?.let {
            followersText.text = profile.followersCount.toString()
        } ?: run {
            followersText.isVisible = false
            followersImg.isVisible = false
            followersLabel.isVisible = false
        }

        detailedInfoLabel.setOnClickListener {
            profileClickListener.onDetailsClick(profile)
        }
    }

    companion object {
        fun create(
            profileClickListener: ProfileAdapter.ProfileClickListener,
            parent: ViewGroup
        ): ProfileViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_profile, parent, false)
            return ProfileViewHolder(profileClickListener, view)
        }

        const val TODAY = "Сегодня"
        const val YESTERDAY = "Вчера"
        const val PATTERN = "dd MMMM YYYY HH:mm"
        const val OFFSET_HOURS = 4
    }

    private val dateFormat: DateTimeFormatter = DateTimeFormat.forPattern(PATTERN)

    private fun formatDate(date: DateTime): String {
        val currentDate = DateTime.now().toDateTime(DateTimeZone.forOffsetHours(OFFSET_HOURS))
        return when {
            currentDate.dayOfYear == date.dayOfYear -> {
                TODAY + " в " + date.hourOfDay().asString + "." + date.minuteOfHour().asString
            }
            (currentDate.dayOfYear - date.dayOfYear) == 1 -> {
                YESTERDAY + " в " + date.hourOfDay().asString + "." + date.minuteOfHour()
                    .asString
            }
            else -> date.toString(dateFormat)
        }
    }
}
