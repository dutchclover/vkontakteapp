package com.example.postlayoutproject.ui.holders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isGone
import com.example.postlayoutproject.R
import com.example.postlayoutproject.data.models.Post
import kotlinx.android.synthetic.main.post_image.view.*

class PostTextViewHolder(itemView: View) : BaseViewHolder(itemView) {

    override fun bind(post: Post) {
        super.bind(post)
        itemView.postImg.isGone = true
    }

    companion object {
        fun create(parent: ViewGroup): PostTextViewHolder {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_post, parent, false)
            return PostTextViewHolder(view)
        }
    }
}