package com.example.postlayoutproject.ui.state

import android.graphics.Bitmap
import android.net.Uri
import androidx.paging.PagingData
import com.example.postlayoutproject.data.models.Post
import com.example.postlayoutproject.data.models.UserPost
import com.example.postlayoutproject.data.models.datatransferobjects.Comment
import com.example.postlayoutproject.data.models.datatransferobjects.Profile
import com.example.postlayoutproject.viewmodels.PostViewModel.UiModel

data class State(
    val userPost: PagingData<UserPost> = PagingData.empty(),
    val post: PagingData<UiModel> = PagingData.empty(),
    val comment: PagingData<Comment> = PagingData.empty(),
    val profile: Profile? = null,
    val isEmptyState: Boolean = false,
    val isLogging: Boolean = false,
    val isLogged: Boolean = false,
    val error: Throwable? = null,
    val isLoading: Boolean = false,
    val isLoadingProfile: Boolean = false,
    val isLoadingUserPosts: Boolean = false,
    val dataLoaded: Boolean = false,
    val profileLoaded: Boolean = false,
    val userPostsLoaded: Boolean = false,
    val hasFavorites: Boolean = false,
    val firstPage: Boolean = false
)

sealed class Action {
    data class PostClicked(val post: Post) : Action()
    data class ChangeFavorite(val postId: Int, val ownerId: Int, val isFavorite: Boolean) : Action()
    data class DeletePost(val postId: Int, val ownerId: Int) : Action()
    data class Logged(val token: String) : Action()
    data class SendComment(val message: String, val ownerId: Int, val postId: Int) : Action()
    data class ShareImage(val bitmap: Bitmap) : Action()
    data class CreatePost(val message: String) : Action()
    object FetchData : Action()
    object Logging : Action()
    object ShowFavoritePosts : Action()
    object ShowPosts : Action()
    object ShowUserPosts : Action()
    object ShowProfile : Action()
    data class ShowDetailedProfile(val profile: Profile) : Action()
}

sealed class Effect {
    data class ShowDetails(val post: Post) : Effect()
    object ShowFavoritePosts : Effect()
    object ShowPosts : Effect()
    object ShowProfile: Effect()
    object ScrollToTop: Effect()
    data class ShowProfileDetails(val profile: Profile): Effect()
    data class SharedImageUri(val uri: Uri?=null): Effect()
}