package com.example.postlayoutproject.di.repository

import android.content.SharedPreferences
import com.example.postlayoutproject.data.database.dao.PostDao
import com.example.postlayoutproject.data.network.TokenStorage
import com.example.postlayoutproject.data.network.VkApiService
import com.example.postlayoutproject.data.repo.VkPostsRepository
import dagger.Module
import dagger.Provides
import io.reactivex.subjects.PublishSubject

@Module
class VkRepositoryModule {
    @Provides
    fun provideVkPostsRepository(
        postDao: PostDao,
        vkApiService: VkApiService,
        tokenStorage: TokenStorage,
        sharedPreferences: SharedPreferences,
        favoriteSubject: PublishSubject<Boolean>,
    ): VkPostsRepository = VkPostsRepository(
        postDao,
        vkApiService,
        tokenStorage,
        sharedPreferences,
        favoriteSubject
    )
}
