package com.example.postlayoutproject.di.screens

import androidx.lifecycle.ViewModel
import com.example.postlayoutproject.di.ViewModelBuilder
import com.example.postlayoutproject.di.ViewModelKey
import com.example.postlayoutproject.ui.MainActivity
import com.example.postlayoutproject.viewmodels.MainActivityViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class MainActivityModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    abstract fun bindMainActivityViewModel(viewModel: MainActivityViewModel): ViewModel

    @ContributesAndroidInjector(modules = [ViewModelBuilder::class])
    internal abstract fun mainActivityInjector(): MainActivity
}
