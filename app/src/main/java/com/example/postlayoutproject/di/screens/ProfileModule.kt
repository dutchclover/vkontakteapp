package com.example.postlayoutproject.di.screens

import androidx.lifecycle.ViewModel
import com.example.postlayoutproject.di.ViewModelBuilder
import com.example.postlayoutproject.di.ViewModelKey
import com.example.postlayoutproject.ui.fragments.ProfileDetailedFragment
import com.example.postlayoutproject.ui.fragments.ProfileFragment
import com.example.postlayoutproject.viewmodels.ProfileViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class ProfileModule {

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    abstract fun bindProfileViewModel(viewModel: ProfileViewModel): ViewModel

    @ContributesAndroidInjector(modules = [ViewModelBuilder::class])
    internal abstract fun profileFragmentInjector(): ProfileFragment

    @ContributesAndroidInjector(modules = [ViewModelBuilder::class])
    internal abstract fun profileDetailedFragmentInjector(): ProfileDetailedFragment
}