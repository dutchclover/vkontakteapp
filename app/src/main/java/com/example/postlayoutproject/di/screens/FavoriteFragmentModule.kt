package com.example.postlayoutproject.di.screens

import androidx.lifecycle.ViewModel
import com.example.postlayoutproject.di.ViewModelBuilder
import com.example.postlayoutproject.di.ViewModelKey
import com.example.postlayoutproject.ui.fragments.FavoritePostsFragment
import com.example.postlayoutproject.viewmodels.FavoritesViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class FavoriteFragmentModule {

    @Binds
    @IntoMap
    @ViewModelKey(FavoritesViewModel::class)
    abstract fun bindFavoritesViewModel(viewModel: FavoritesViewModel): ViewModel

    @ContributesAndroidInjector(modules = [ViewModelBuilder::class])
    internal abstract fun favoritePostsFragmentInjector(): FavoritePostsFragment
}
