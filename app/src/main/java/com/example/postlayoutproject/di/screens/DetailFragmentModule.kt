package com.example.postlayoutproject.di.screens

import androidx.lifecycle.ViewModel
import com.example.postlayoutproject.di.ViewModelBuilder
import com.example.postlayoutproject.di.ViewModelKey
import com.example.postlayoutproject.ui.fragments.PostDetailFragment
import com.example.postlayoutproject.viewmodels.DetailViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class DetailFragmentModule {

    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    abstract fun bindDetailViewModel(viewModel: DetailViewModel): ViewModel

    @ContributesAndroidInjector(modules = [ViewModelBuilder::class])
    internal abstract fun detailFragmentInjector(): PostDetailFragment
}
