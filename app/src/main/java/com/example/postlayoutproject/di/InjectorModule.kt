package com.example.postlayoutproject.di

import com.example.postlayoutproject.di.screens.*
import dagger.Module

@Module(
    includes = [
        MainActivityModule::class,
        NavigationFragmentModule::class,
        FavoriteFragmentModule::class,
        PostsFragmentModule::class,
        DetailFragmentModule::class,
        ProfileModule::class,
        CreatePostActivityModule::class
    ]
)
abstract class InjectorModule
