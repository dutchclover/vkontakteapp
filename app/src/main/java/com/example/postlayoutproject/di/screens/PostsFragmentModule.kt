package com.example.postlayoutproject.di.screens

import androidx.lifecycle.ViewModel
import com.example.postlayoutproject.di.ViewModelBuilder
import com.example.postlayoutproject.di.ViewModelKey
import com.example.postlayoutproject.ui.fragments.PostsFragment
import com.example.postlayoutproject.viewmodels.PostViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class PostsFragmentModule {

    @Binds
    @IntoMap
    @ViewModelKey(PostViewModel::class)
    abstract fun bindPostViewModel(viewModel: PostViewModel): ViewModel

    @ContributesAndroidInjector(modules = [ViewModelBuilder::class])
    internal abstract fun postsFragmentInjector(): PostsFragment
}
