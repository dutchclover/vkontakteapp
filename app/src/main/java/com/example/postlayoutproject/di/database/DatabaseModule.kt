package com.example.postlayoutproject.di.database

import android.content.Context
import androidx.room.Room
import com.example.postlayoutproject.data.database.DATABASE_NAME
import com.example.postlayoutproject.data.database.PostsDatabase
import com.example.postlayoutproject.data.database.dao.PostDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun providesRoomDatabase(context: Context): PostsDatabase =
        Room.databaseBuilder(
            context.applicationContext,
            PostsDatabase::class.java,
            DATABASE_NAME,
        ).fallbackToDestructiveMigration()
            .build()

    @Provides
    @Singleton
    fun providePostDao(postsDatabase: PostsDatabase): PostDao = postsDatabase.postDao
}
