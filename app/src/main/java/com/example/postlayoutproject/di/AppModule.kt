package com.example.postlayoutproject.di

import android.content.Context
import android.content.SharedPreferences
import com.example.postlayoutproject.App
import com.example.postlayoutproject.di.apiservices.ApiModule
import com.example.postlayoutproject.di.repository.VkRepositoryModule
import dagger.Module
import dagger.Provides
import io.reactivex.subjects.PublishSubject
import javax.inject.Singleton

private const val PREFS = "prefs"

/**
 * Global module includes all singletons for the application.
 *
 */
@Module(
    includes = [
        VkRepositoryModule::class,
        ApiModule::class,
        InjectorModule::class
    ]
)
class AppModule {

    @Provides
    @Singleton
    fun provideApp(app: App): Context = app

    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences =
        context.getSharedPreferences(PREFS, Context.MODE_PRIVATE)

    @Provides
    @Singleton
    fun provideFavoriteSubject(): PublishSubject<Boolean> = PublishSubject.create()
}

