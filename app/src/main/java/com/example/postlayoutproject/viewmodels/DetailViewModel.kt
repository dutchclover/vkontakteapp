package com.example.postlayoutproject.viewmodels


import android.graphics.Bitmap
import androidx.lifecycle.viewModelScope
import androidx.paging.rxjava2.cachedIn
import com.example.postlayoutproject.data.models.Post
import com.example.postlayoutproject.data.repo.VkPostsRepository
import com.example.postlayoutproject.saveBitmapForSharing
import com.example.postlayoutproject.ui.state.Action
import com.example.postlayoutproject.ui.state.Effect
import com.example.postlayoutproject.ui.state.State
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class DetailViewModel @Inject constructor(private val vkPostsRepository: VkPostsRepository) :
    BaseViewModel() {

    init {
        viewState = State(isEmptyState = true, isLoading = false)
    }

    private fun getComments(post: Post): Disposable =
        vkPostsRepository.fetchComments(post)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .cachedIn(viewModelScope)
            .subscribe({
                viewState = viewState.copy(isLoading = false, comment = it, dataLoaded = true)
            }, {
                viewState = viewState.copy(error = it, isLoading = false)
                it.printStackTrace()
            })
            .also {
                viewState = viewState.copy(isLoading = true, isEmptyState = false, dataLoaded = false)
                compositeDisposable.add(it)
            }

    override fun process(viewAction: Action) {
        super.process(viewAction)
        when (viewAction) {
            is Action.PostClicked -> {
                getComments(viewAction.post)
            }
            is Action.SendComment -> sendComment(
                viewAction.message,
                viewAction.ownerId,
                viewAction.postId
            )
            is Action.ShareImage -> prepareBitmapForSharing(viewAction.bitmap)
        }
    }

    private fun sendComment(message: String, ownerId: Int, postId: Int) {
        val uniqueID = UUID.randomUUID().toString()
        vkPostsRepository.publishComment(message, ownerId, postId, uniqueID)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map {
                it.response.commentId
            }
            .subscribe({
            }, {
                it.printStackTrace()
            }).also {
                compositeDisposable.add(it)
            }
    }

    private fun prepareBitmapForSharing(bitmap: Bitmap) = Single.fromCallable {
        return@fromCallable bitmap.saveBitmapForSharing()
    }.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({
            viewEffect = Effect.SharedImageUri(it)
        }, {
            viewEffect = Effect.SharedImageUri()
        })
}