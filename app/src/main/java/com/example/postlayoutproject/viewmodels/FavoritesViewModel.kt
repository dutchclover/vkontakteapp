package com.example.postlayoutproject.viewmodels

import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.insertSeparators
import androidx.paging.map
import androidx.paging.rxjava2.cachedIn
import com.example.postlayoutproject.data.database.DatabasePost
import com.example.postlayoutproject.data.database.asDomainModel
import com.example.postlayoutproject.data.repo.VkPostsRepository
import com.example.postlayoutproject.ui.state.Action
import com.example.postlayoutproject.ui.state.State
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.joda.time.DateTime
import javax.inject.Inject

class FavoritesViewModel @Inject constructor(vkPostsRepository: VkPostsRepository) : PostViewModel(vkPostsRepository) {

    init {
        viewState = State(isEmptyState = true, isLoading = false)
    }

    override fun process(viewAction: Action) {
        super.process(viewAction)
        when (viewAction) {
            is Action.FetchData -> getPosts()
            is Action.DeletePost -> super.removePost(viewAction.postId, viewAction.ownerId)
            is Action.ChangeFavorite -> setIsFavorite(
                viewAction.postId,
                viewAction.ownerId,
                viewAction.isFavorite
            )
        }
    }

    override fun getPosts(): Disposable =
        requestFavoritePosts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { pagingData -> pagingData.map { UiModel.PostItem(it.asDomainModel()) } }
            .map {
                it.insertSeparators { before, after ->
                    if (after == null) {
                        return@insertSeparators null
                    }
                    if (before == null) {
                        return@insertSeparators null
                    }
                    if (DateTime.parse(after.post.formattedDate, dateFormat).dayOfYear()
                            .get() == DateTime.parse(before.post.formattedDate, dateFormat).dayOfYear()
                            .get()
                    ) {
                        return@insertSeparators null
                    } else {
                        return@insertSeparators UiModel.SeparatorItem(
                            formatDate(
                                DateTime.parse(
                                    after.post.formattedDate,
                                    dateFormat
                                )
                            )
                        )
                    }
                }
            }
            .cachedIn(viewModelScope)
            .subscribe({
                viewState = viewState.copy(isLoading = false, post = it, dataLoaded = true)
                vkPostsRepository.invalidateHasFavorites()
                viewState = viewState.copy(isLoading = true, isEmptyState = false)
            },
                {
                    viewState = viewState.copy(error = it, isLoading = false)
                }).also {

                viewState = viewState.copy(isLoading = true, isEmptyState = false)
                compositeDisposable.add(it)
            }

    private fun requestFavoritePosts(): Flowable<PagingData<DatabasePost>> =
        vkPostsRepository.getFavoritePosts()
}