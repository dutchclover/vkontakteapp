package com.example.postlayoutproject.viewmodels

import com.example.postlayoutproject.data.network.TokenStorage
import com.example.postlayoutproject.ui.state.Action
import com.example.postlayoutproject.ui.state.State
import com.vk.api.sdk.VK
import javax.inject.Inject

class MainActivityViewModel @Inject constructor(private val tokenStorage: TokenStorage) : BaseViewModel() {

    init {
        viewState = State(isEmptyState = true)
    }

    private fun isLogged() = VK.isLoggedIn()

    override fun process(viewAction: Action) {
        super.process(viewAction)
        when (viewAction) {
            is Action.Logging -> {
                viewState = if (isLogged()) {
                    viewState.copy(isLogging = false, isEmptyState = false, isLogged = true)
                } else {
                    viewState.copy(isLogging = true, isEmptyState = false, isLogged = false)
                }
            }
            is Action.Logged -> {
                tokenStorage.saveToken(viewAction.token)
                viewState = viewState.copy(isLogging = false, isEmptyState = false, isLogged = true)
            }
        }
    }
}
