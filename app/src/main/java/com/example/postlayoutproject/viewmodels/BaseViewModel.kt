package com.example.postlayoutproject.viewmodels

import com.example.postlayoutproject.ui.state.Action
import com.example.postlayoutproject.ui.state.Effect

import com.example.postlayoutproject.ui.state.State
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel :  MviViewModel<State, Effect, Action>(){
    protected val compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}