package com.example.postlayoutproject.viewmodels

import com.example.postlayoutproject.ui.state.Action
import com.example.postlayoutproject.ui.state.Effect
import com.example.postlayoutproject.ui.state.State
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class NavigationViewModel @Inject constructor(favoriteSubject: PublishSubject<Boolean>) : BaseViewModel() {

    init {
        viewState = State()
        favoriteSubject.subscribe {
            viewState = viewState.copy(hasFavorites = it, isEmptyState = false)
        }.also {
            compositeDisposable.add(it)
        }
    }

    override fun process(viewAction: Action) {
        super.process(viewAction)
        when (viewAction) {
            is Action.ShowPosts -> showPosts()
            is Action.ShowFavoritePosts -> showFavoritePosts()
            is Action.ShowProfile -> showProfile()
        }
    }

    private fun showProfile() {
        viewEffect = Effect.ShowProfile
    }

    private fun showPosts() {
        viewEffect = Effect.ShowPosts
    }

    private fun showFavoritePosts() {
        viewEffect = Effect.ShowFavoritePosts
    }
}