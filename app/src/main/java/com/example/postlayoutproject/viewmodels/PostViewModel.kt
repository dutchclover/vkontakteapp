package com.example.postlayoutproject.viewmodels

import androidx.lifecycle.viewModelScope
import androidx.paging.insertSeparators
import androidx.paging.map
import androidx.paging.rxjava2.cachedIn
import com.example.postlayoutproject.data.database.asDomainModel
import com.example.postlayoutproject.data.models.Post
import com.example.postlayoutproject.data.repo.VkPostsRepository
import com.example.postlayoutproject.ui.state.Action
import com.example.postlayoutproject.ui.state.Effect
import com.example.postlayoutproject.ui.state.State
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import java.util.*
import javax.inject.Inject

open class PostViewModel @Inject constructor(protected val vkPostsRepository: VkPostsRepository) : BaseViewModel() {

    init {
        viewState = State(isEmptyState = true, isLoading = false)
    }

    override fun process(viewAction: Action) {
        super.process(viewAction)
        when (viewAction) {
            is Action.FetchData -> getPosts()
            is Action.DeletePost -> removePost(viewAction.postId, viewAction.ownerId)
            is Action.ChangeFavorite -> setIsFavorite(
                viewAction.postId,
                viewAction.ownerId,
                viewAction.isFavorite
            )
            is Action.PostClicked -> postClicked(viewAction.post)
        }
    }

    private fun postClicked(post: Post) {
        viewEffect = Effect.ShowDetails(post)
    }

    protected open fun getPosts(): Disposable = getUiModel()
        .cachedIn(viewModelScope)
        .subscribe({
            viewState = viewState.copy(isLoading = false, post = it, dataLoaded = true)
            vkPostsRepository.invalidateHasFavorites()
        }, {
            viewState = viewState.copy(error = it, isLoading = false)
            it.printStackTrace()
        })
        .also {
            viewState = viewState.copy(isLoading = true, isEmptyState = false)
            compositeDisposable.add(it)
        }

    open fun removePost(postId: Int, ownerId: Int) {
        vkPostsRepository.removePostFromDb(postId)
            .andThen(vkPostsRepository.removePost(postId, ownerId))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
            }) {
                it.printStackTrace()
            }.also {
                compositeDisposable.add(it)
            }
        requestCache()
    }

    open fun setIsFavorite(postId: Int, ownerId: Int, isFavorite: Boolean) {
        vkPostsRepository.setFavorite(postId, ownerId, isFavorite)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                requestCache()
            }, {
                it.printStackTrace()
            }).also {
                compositeDisposable.add(it)
            }
    }

    protected open fun requestCache() {
        getPosts()
    }

    sealed class UiModel {
        data class PostItem(val post: Post) : UiModel()
        data class SeparatorItem(val description: String) : UiModel()
    }

    private fun getUiModel() = vkPostsRepository.fetchPosts()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .map { pagingData -> pagingData.map { UiModel.PostItem(it.asDomainModel()) } }
        .map {
            it.insertSeparators { before, after ->
                if (after == null) {
                    return@insertSeparators null
                }
                if (before == null) {
                    return@insertSeparators null
                }
                if (DateTime.parse(after.post.formattedDate, dateFormat).dayOfYear().get()
                         == DateTime.parse(before.post.formattedDate, dateFormat).dayOfYear().get()

                ) {
                    return@insertSeparators null
                } else {
                    return@insertSeparators UiModel.SeparatorItem(
                        formatDate(
                            DateTime.parse(
                                after.post.formattedDate,
                                dateFormat
                            )
                        )
                    )
                }
            }
        }

    companion object {
        const val YESTERDAY = "Вчера"
        const val PATTERN = "dd MMMM YYYY HH:mm"
        const val RUSSIAN_LOCALE = "ru"
    }

    val dateFormat: DateTimeFormatter = DateTimeFormat.forPattern(PATTERN).withLocale(Locale(
        RUSSIAN_LOCALE))

    fun formatDate(date: DateTime): String {
        val currentDate = DateTime.now()
        return when (currentDate.dayOfYear) {
            date.dayOfYear -> YESTERDAY
            else -> date.toString(dateFormat)
        }
    }
}
