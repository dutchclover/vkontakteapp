package com.example.postlayoutproject.viewmodels

import android.util.Log
import androidx.annotation.CallSuper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.postlayoutproject.NoObserverAttachedException
import com.example.postlayoutproject.TAG
import com.example.postlayoutproject.ViewModelContract
import com.example.postlayoutproject.ui.state.SingleLiveEvent

open class MviViewModel<STATE, EFFECT, ACTION> :
    ViewModel(), ViewModelContract<ACTION> {

    private val _viewStates: MutableLiveData<STATE> = MutableLiveData()
    fun viewStates(): LiveData<STATE> = _viewStates

    private var _viewState: STATE? = null
    protected var viewState: STATE
        get() = _viewState
            ?: throw UninitializedPropertyAccessException(UNINITIALIZED_STATE_EXCEPTION)
        set(value) {
            Log.d(TAG, SETTING_VIEWSTATE + "$value")
            _viewState = value
            _viewStates.postValue(value)
        }

    private val _viewEffects: SingleLiveEvent<EFFECT> = SingleLiveEvent()
    fun viewEffects(): SingleLiveEvent<EFFECT> = _viewEffects

    private var _viewEffect: EFFECT? = null
    protected var viewEffect: EFFECT
        get() = _viewEffect
            ?: throw UninitializedPropertyAccessException(UNINITIALIZED_EFFECT_EXCEPTION)
        set(value) {
            Log.d(TAG, SETTING_VIEWEFFECT + "$value")
            _viewEffect = value
            _viewEffects.value = value
        }

    @CallSuper
    override fun process(viewAction: ACTION) {
        if (!viewStates().hasObservers()) {
            throw NoObserverAttachedException(NO_OBSERVER_ATTACHED)
        }
        Log.d(TAG, PROCESSING_VIEWACTION + "$viewAction")
    }

    override fun onCleared() {
        super.onCleared()
        Log.d(TAG, CLEARED)
    }

    companion object {
        const val CLEARED = "onCleared"
        const val NO_OBSERVER_ATTACHED =
            "No observer attached. In case of custom View \"startObserving()\" function needs to be called manually."
        const val PROCESSING_VIEWACTION = "processing viewAction:"
        const val SETTING_VIEWEFFECT = "setting viewEffect:"
        const val SETTING_VIEWSTATE = "setting viewState:"
        const val UNINITIALIZED_EFFECT_EXCEPTION =
            "\"viewEffect\" was queried before being initialized"
        const val UNINITIALIZED_STATE_EXCEPTION =
            "\"viewState\" was queried before being initialized"
    }
}