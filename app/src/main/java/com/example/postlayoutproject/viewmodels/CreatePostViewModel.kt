package com.example.postlayoutproject.viewmodels


import android.content.Context
import com.example.postlayoutproject.data.repo.VkPostsRepository
import com.example.postlayoutproject.ui.state.Action
import com.example.postlayoutproject.ui.state.State
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class CreatePostViewModel @Inject constructor(
    private val vkPostsRepository: VkPostsRepository,
    val context: Context
) : BaseViewModel() {

    init {
        viewState = State(isEmptyState = true, isLoading = false)
    }

    private fun sendPost(message: String) {
        val uniqueID = UUID.randomUUID().toString()
        vkPostsRepository.publishPost(message, uniqueID)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map {
                it.response.postId
            }
            .subscribe(object : SingleObserver<Int> {
                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

                override fun onSuccess(id: Int) {
                    viewState = viewState.copy(dataLoaded = true, isLoading = false)
                }

                override fun onError(e: Throwable) {
                    viewState = viewState.copy(error = e, isLoading = false)
                    e.printStackTrace()
                }
            })
    }

    override fun process(viewAction: Action) {
        super.process(viewAction)
        when (viewAction) {
            is Action.CreatePost -> sendPost(viewAction.message)
        }
    }
}