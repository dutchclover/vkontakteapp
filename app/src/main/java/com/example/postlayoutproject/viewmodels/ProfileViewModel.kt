package com.example.postlayoutproject.viewmodels

import androidx.lifecycle.viewModelScope
import androidx.paging.rxjava2.cachedIn
import com.example.postlayoutproject.data.repo.VkPostsRepository
import com.example.postlayoutproject.ui.state.Action
import com.example.postlayoutproject.ui.state.Effect
import com.example.postlayoutproject.ui.state.State
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ProfileViewModel @Inject constructor(private val vkPostsRepository: VkPostsRepository) : BaseViewModel() {

    init {
        viewState = State(isEmptyState = true, isLoadingProfile = false, isLoadingUserPosts = false)
    }

    private fun getProfile(): Disposable =
        vkPostsRepository.fetchProfile()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { profileList ->
                return@map profileList.first()
            }
            .subscribe({
                viewState = viewState.copy(isLoadingProfile = false, profile = it, profileLoaded = true)
            }, {
                viewState = viewState.copy(error = it, isLoadingProfile = false)
                it.printStackTrace()
            })
            .also {
                viewState =
                    viewState.copy(
                        isLoadingProfile = true,
                        isEmptyState = false,
                        profileLoaded = false
                    )
                compositeDisposable.add(it)
            }

    private fun getUserPosts(): Disposable =
        vkPostsRepository.getUserPosts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .cachedIn(viewModelScope)
            .subscribe({
                viewState = viewState.copy(
                    isLoadingUserPosts = false,
                    userPost = it,
                    userPostsLoaded = true
                )
            }, {
                viewState = viewState.copy(error = it, isLoadingUserPosts = false)
                it.printStackTrace()
            })
            .also {
                viewState = viewState.copy(
                    isLoadingUserPosts = true,
                    isEmptyState = false,
                    userPostsLoaded = false,
                    profileLoaded = false
                )
                compositeDisposable.add(it)
            }

    override fun process(viewAction: Action) {
        super.process(viewAction)
        when (viewAction) {
            is Action.ShowProfile -> {
                getProfile()
            }
            is Action.ShowDetailedProfile -> {
                viewEffect = Effect.ShowProfileDetails(viewAction.profile)
            }
            is Action.ShowUserPosts -> {
                viewEffect = Effect.ScrollToTop
                getUserPosts()
            }
        }
    }
}