package com.example.postlayoutproject

import com.example.postlayoutproject.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class App : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    override fun applicationInjector(): AndroidInjector<App> = DaggerAppComponent.builder()
        .application(this)
        .build()

    companion object {
        lateinit var instance: App
            private set
    }
}